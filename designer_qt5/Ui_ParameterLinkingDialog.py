# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Ui_ParameterLinkingDialog.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_ParameterLinkingDialog(object):
    def setupUi(self, ParameterLinkingDialog):
        ParameterLinkingDialog.setObjectName("ParameterLinkingDialog")
        ParameterLinkingDialog.setWindowModality(QtCore.Qt.ApplicationModal)
        ParameterLinkingDialog.resize(400, 500)
        self.verticalLayout = QtWidgets.QVBoxLayout(ParameterLinkingDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.toolButtonAdd = QtWidgets.QToolButton(ParameterLinkingDialog)
        self.toolButtonAdd.setMinimumSize(QtCore.QSize(32, 32))
        self.toolButtonAdd.setMaximumSize(QtCore.QSize(32, 32))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/MainWindow/add.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.toolButtonAdd.setIcon(icon)
        self.toolButtonAdd.setObjectName("toolButtonAdd")
        self.horizontalLayout.addWidget(self.toolButtonAdd)
        self.toolButtonDelete = QtWidgets.QToolButton(ParameterLinkingDialog)
        self.toolButtonDelete.setMinimumSize(QtCore.QSize(32, 32))
        self.toolButtonDelete.setMaximumSize(QtCore.QSize(32, 32))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/MainWindow/close.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.toolButtonDelete.setIcon(icon1)
        self.toolButtonDelete.setObjectName("toolButtonDelete")
        self.horizontalLayout.addWidget(self.toolButtonDelete)
        spacerItem = QtWidgets.QSpacerItem(40, 24, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.toolButtonHelp = QtWidgets.QToolButton(ParameterLinkingDialog)
        self.toolButtonHelp.setMinimumSize(QtCore.QSize(32, 32))
        self.toolButtonHelp.setMaximumSize(QtCore.QSize(32, 32))
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/MainWindow/help.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.toolButtonHelp.setIcon(icon2)
        self.toolButtonHelp.setObjectName("toolButtonHelp")
        self.horizontalLayout.addWidget(self.toolButtonHelp)
        self.toolButtonClose = QtWidgets.QToolButton(ParameterLinkingDialog)
        self.toolButtonClose.setMinimumSize(QtCore.QSize(32, 32))
        self.toolButtonClose.setMaximumSize(QtCore.QSize(32, 32))
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/MainWindow/exit.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.toolButtonClose.setIcon(icon3)
        self.toolButtonClose.setObjectName("toolButtonClose")
        self.horizontalLayout.addWidget(self.toolButtonClose)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.labelCurrentlyLinked = QtWidgets.QLabel(ParameterLinkingDialog)
        self.labelCurrentlyLinked.setObjectName("labelCurrentlyLinked")
        self.verticalLayout.addWidget(self.labelCurrentlyLinked)
        self.listWidget = QtWidgets.QListWidget(ParameterLinkingDialog)
        self.listWidget.setObjectName("listWidget")
        self.verticalLayout.addWidget(self.listWidget)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setContentsMargins(-1, 10, -1, -1)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.pushButtonClose = QtWidgets.QPushButton(ParameterLinkingDialog)
        self.pushButtonClose.setObjectName("pushButtonClose")
        self.horizontalLayout_2.addWidget(self.pushButtonClose)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.retranslateUi(ParameterLinkingDialog)
        QtCore.QMetaObject.connectSlotsByName(ParameterLinkingDialog)
        ParameterLinkingDialog.setTabOrder(self.toolButtonAdd, self.toolButtonDelete)
        ParameterLinkingDialog.setTabOrder(self.toolButtonDelete, self.toolButtonClose)
        ParameterLinkingDialog.setTabOrder(self.toolButtonClose, self.pushButtonClose)

    def retranslateUi(self, ParameterLinkingDialog):
        _translate = QtCore.QCoreApplication.translate
        ParameterLinkingDialog.setWindowTitle(_translate("ParameterLinkingDialog", "Parameter linking..."))
        self.toolButtonAdd.setToolTip(_translate("ParameterLinkingDialog", "Add new parameter link"))
        self.toolButtonAdd.setText(_translate("ParameterLinkingDialog", "..."))
        self.toolButtonDelete.setToolTip(_translate("ParameterLinkingDialog", "Remove selected parameter link"))
        self.toolButtonDelete.setText(_translate("ParameterLinkingDialog", "..."))
        self.toolButtonHelp.setToolTip(_translate("ParameterLinkingDialog", "Online Help"))
        self.toolButtonHelp.setText(_translate("ParameterLinkingDialog", "..."))
        self.toolButtonClose.setToolTip(_translate("ParameterLinkingDialog", "Close Dialog"))
        self.toolButtonClose.setText(_translate("ParameterLinkingDialog", "..."))
        self.labelCurrentlyLinked.setText(_translate("ParameterLinkingDialog", "Currently linked:"))
        self.pushButtonClose.setText(_translate("ParameterLinkingDialog", "Close"))
import PaXoverNG_rc
