# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Ui_EditOutputDialog.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_EditOutputDialog(object):
    def setupUi(self, EditOutputDialog):
        EditOutputDialog.setObjectName("EditOutputDialog")
        EditOutputDialog.setWindowModality(QtCore.Qt.ApplicationModal)
        EditOutputDialog.resize(488, 401)
        EditOutputDialog.setMinimumSize(QtCore.QSize(400, 200))
        EditOutputDialog.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.scrollArea = QtWidgets.QScrollArea(EditOutputDialog)
        self.scrollArea.setGeometry(QtCore.QRect(10, 10, 471, 381))
        self.scrollArea.setMinimumSize(QtCore.QSize(0, 0))
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 469, 379))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.gridLayoutWidget = QtWidgets.QWidget(self.scrollAreaWidgetContents)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 10, 446, 111))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.labelOutputSinkName = QtWidgets.QLabel(self.gridLayoutWidget)
        self.labelOutputSinkName.setObjectName("labelOutputSinkName")
        self.gridLayout.addWidget(self.labelOutputSinkName, 0, 0, 1, 1)
        self.comboBoxOutputDevice = QtWidgets.QComboBox(self.gridLayoutWidget)
        self.comboBoxOutputDevice.setObjectName("comboBoxOutputDevice")
        self.gridLayout.addWidget(self.comboBoxOutputDevice, 0, 1, 1, 1)
        self.comboBoxOutputSampleRate = QtWidgets.QComboBox(self.gridLayoutWidget)
        self.comboBoxOutputSampleRate.setObjectName("comboBoxOutputSampleRate")
        self.gridLayout.addWidget(self.comboBoxOutputSampleRate, 1, 1, 1, 1)
        self.labelOutputSampleRate = QtWidgets.QLabel(self.gridLayoutWidget)
        self.labelOutputSampleRate.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.labelOutputSampleRate.setObjectName("labelOutputSampleRate")
        self.gridLayout.addWidget(self.labelOutputSampleRate, 1, 0, 1, 1)
        self.labelOutputSampleFormat = QtWidgets.QLabel(self.gridLayoutWidget)
        self.labelOutputSampleFormat.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.labelOutputSampleFormat.setObjectName("labelOutputSampleFormat")
        self.gridLayout.addWidget(self.labelOutputSampleFormat, 2, 0, 1, 1)
        self.comboBoxOutputSampleFormat = QtWidgets.QComboBox(self.gridLayoutWidget)
        self.comboBoxOutputSampleFormat.setObjectName("comboBoxOutputSampleFormat")
        self.gridLayout.addWidget(self.comboBoxOutputSampleFormat, 2, 1, 1, 1)
        self.gridLayout.setColumnStretch(1, 1)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)

        self.retranslateUi(EditOutputDialog)
        QtCore.QMetaObject.connectSlotsByName(EditOutputDialog)

    def retranslateUi(self, EditOutputDialog):
        _translate = QtCore.QCoreApplication.translate
        EditOutputDialog.setWindowTitle(_translate("EditOutputDialog", "Dialog"))
        self.labelOutputSinkName.setText(_translate("EditOutputDialog", "Output Device:"))
        self.labelOutputSampleRate.setText(_translate("EditOutputDialog", "Samplerate:"))
        self.labelOutputSampleFormat.setText(_translate("EditOutputDialog", "Sample Format:"))
