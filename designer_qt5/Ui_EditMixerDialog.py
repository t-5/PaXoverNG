# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Ui_EditMixerDialog.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_EditMixerDialog(object):
    def setupUi(self, EditMixerDialog):
        EditMixerDialog.setObjectName("EditMixerDialog")
        EditMixerDialog.setWindowModality(QtCore.Qt.ApplicationModal)
        EditMixerDialog.resize(500, 608)
        EditMixerDialog.setMinimumSize(QtCore.QSize(400, 200))
        EditMixerDialog.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.scrollArea = QtWidgets.QScrollArea(EditMixerDialog)
        self.scrollArea.setGeometry(QtCore.QRect(5, 9, 490, 591))
        self.scrollArea.setMinimumSize(QtCore.QSize(0, 0))
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 488, 589))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)

        self.retranslateUi(EditMixerDialog)
        QtCore.QMetaObject.connectSlotsByName(EditMixerDialog)

    def retranslateUi(self, EditMixerDialog):
        _translate = QtCore.QCoreApplication.translate
        EditMixerDialog.setWindowTitle(_translate("EditMixerDialog", "Dialog"))
