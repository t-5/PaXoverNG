# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Ui_EditInputDialog.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_EditInputDialog(object):
    def setupUi(self, EditInputDialog):
        EditInputDialog.setObjectName("EditInputDialog")
        EditInputDialog.resize(350, 140)
        self.buttonBox = QtWidgets.QDialogButtonBox(EditInputDialog)
        self.buttonBox.setGeometry(QtCore.QRect(10, 100, 331, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayoutWidget = QtWidgets.QWidget(EditInputDialog)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 10, 331, 76))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.spinBoxNumChannels = QtWidgets.QSpinBox(self.gridLayoutWidget)
        self.spinBoxNumChannels.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.spinBoxNumChannels.setMaximum(16)
        self.spinBoxNumChannels.setObjectName("spinBoxNumChannels")
        self.gridLayout.addWidget(self.spinBoxNumChannels, 0, 1, 1, 1)
        self.labelNumChannels = QtWidgets.QLabel(self.gridLayoutWidget)
        self.labelNumChannels.setObjectName("labelNumChannels")
        self.gridLayout.addWidget(self.labelNumChannels, 0, 0, 1, 1)
        self.labelInputType = QtWidgets.QLabel(self.gridLayoutWidget)
        self.labelInputType.setObjectName("labelInputType")
        self.gridLayout.addWidget(self.labelInputType, 1, 0, 1, 1)
        self.comboBoxInputType = QtWidgets.QComboBox(self.gridLayoutWidget)
        self.comboBoxInputType.setObjectName("comboBoxInputType")
        self.gridLayout.addWidget(self.comboBoxInputType, 1, 1, 1, 1)

        self.retranslateUi(EditInputDialog)
        self.buttonBox.accepted.connect(EditInputDialog.accept)
        self.buttonBox.rejected.connect(EditInputDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(EditInputDialog)

    def retranslateUi(self, EditInputDialog):
        _translate = QtCore.QCoreApplication.translate
        EditInputDialog.setWindowTitle(_translate("EditInputDialog", "Dialog"))
        self.labelNumChannels.setText(_translate("EditInputDialog", "Number of Channels:"))
        self.labelInputType.setText(_translate("EditInputDialog", "Input System:"))
