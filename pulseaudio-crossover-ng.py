#!/usr/bin/python3

"""
PaXoverNG
=========
Pulseaudio Multi-Way Speaker Crossover Configurator

Main python (startup) file

GIT: https://gitlab.com/t-5/PaXoverNG.git
"""

from PyQt5 import QtWidgets
from qt5_t5darkstyle import darkstyle_css
import sys
from WindowClasses.MainWindow import MainWindow


def main():
    app = QtWidgets.QApplication(sys.argv)
    form = MainWindow()
    form.setStyleSheet(darkstyle_css())
    form.show()
    app.exec_()


if __name__ == '__main__':
    main()
