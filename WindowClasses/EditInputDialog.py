from PyQt5.QtWidgets import QDialog, QWidget
from designer_qt5.Ui_EditInputDialog import Ui_EditInputDialog

from DataClasses import XoverInput
from helpers.constants import INPUT_TYPE_MAPPING, MAX_CHANNELS, MIN_CHANNELS


class EditInputDialog(QDialog, Ui_EditInputDialog):

    def __init__(self, parent: QWidget, _input: XoverInput, newlyCreated:bool=False):
        super().__init__(parent=parent)
        self.setupUi(self)
        self._input = _input
        self._newlyCreated = newlyCreated
        self.setWindowTitle("Edit Input")
        self.spinBoxNumChannels.setValue(self._input.numChannels())
        self.spinBoxNumChannels.setMinimum(MIN_CHANNELS)
        self.spinBoxNumChannels.setMaximum(MAX_CHANNELS)
        self.comboBoxInputType.addItems(INPUT_TYPE_MAPPING.keys())
        for text, type_ in INPUT_TYPE_MAPPING.items():
            if _input.inputType() == type_:
                self.comboBoxInputType.setCurrentText(text)


    def accept(self):
        self._input.setNumChannels(self.spinBoxNumChannels.value())
        self._input.setInputType(INPUT_TYPE_MAPPING[self.comboBoxInputType.currentText()])
        self.close()