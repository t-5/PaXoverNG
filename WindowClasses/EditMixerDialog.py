from PyQt5.QtWidgets import QMessageBox, QWidget

from WindowClasses.AbstractMixingDialog import AbstractMixingDialog
from designer_qt5.Ui_EditMixerDialog import Ui_EditMixerDialog


class EditMixerDialog(AbstractMixingDialog, Ui_EditMixerDialog):

    def __init__(self, parent: QWidget, mixer, newlyCreated:bool=False):
        super().__init__(parent=parent, mixer=mixer, newlyCreated=newlyCreated)
        self.setupUi(self)
        # newly created?
        if newlyCreated:
            self.setWindowTitle("Add new %s..." % mixer.filterName())
        else:
            self.setWindowTitle("Edit %s '%s'" % (mixer.filterName(), self._mixer.sinkName()))
        geo = self.parent().settings().value("main/mixerDialogGeo")
        if geo is not None:
            self.restoreGeometry(geo)
        mixer.markDirty()
        # set up the dialog's widgets
        self._canSetOutputChannels = True
        self._canSetSinkName = True
        self._setupChannelWidgets(newlyCreated)


    def closeEvent(self, event):
        self.parent().settings().setValue("main/mixerDialogGeo", self.saveGeometry())


    def _checkAndSaveSinkName(self):
        name = self._lineEditSinkName.text()
        if not self.parent().isUniqueName(name, exclude=self._mixer):
            QMessageBox().critical(self, "ERROR", "The name '%s' is not unique!" % name)
            return -1
        checkResult = self.parent().checkSinkName(name)
        if checkResult:
            msg = "The name '%s' has errors:\n" % name
            QMessageBox().critical(self, "ERROR", msg + checkResult)
            return -2
        self._mixer.setSinkName(name)
        return 0


    def _onOutputChannelsChanged(self):
        self._mixer.setNumOutputs(self._spinBoxOutputs.value())
        self._setupChannelWidgets(False, True)


    def _readIniSettings(self):
        super()._readIniSettingsFor("editMixerDialog")


    def _writeIniSettings(self):
        """ write ini settings """
        super()._writeIniSettingsFor("editMixerDialog")
