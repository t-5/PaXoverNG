from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QDialog
from designer_qt5.Ui_FrequencyResponseDialog import Ui_FrequencyResponseDialog
from qt5_t5darkstyle import darkstyle_css


class FrequencyResponseDialog(QDialog, Ui_FrequencyResponseDialog):
    def __init__(self, title: str, pixmap: QPixmap):
        super(FrequencyResponseDialog, self).__init__()
        self.setupUi(self)
        self.setStyleSheet(darkstyle_css())
        self.setModal(True)
        self.setWindowTitle(title)
        self.frequencResponseLabel.setPixmap(pixmap)
        self.pushButtonClose.clicked.connect(self.onCloseClicked)


    def onCloseClicked(self):
        self.close()
