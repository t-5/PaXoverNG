from PyQt5.QtCore import QEvent, QPoint, QTimer, Qt
from PyQt5.QtWidgets import QDial, QDialog, QDoubleSpinBox, QLabel, QLineEdit, QPushButton, QSpinBox, QToolButton

from helpers.constants import MAX_CHANNELS, MIN_CHANNELS
from helpers.functions import colorForChannelNumRgbString


class AbstractMixingDialog(QDialog):

    # constants
    VERTICAL_GRID = 120
    HORIZONTAL_GRID = 120
    DIAL_LEFT = 15
    SPINBOX_HEIGHT = 25
    INPUT_LABEL_WIDTH = 35
    TOPMARGIN = 5
    LEFTMARGIN = 5
    BUTTONS_HEIGHT = 35
    MUTE_BUTTON_HEIGHT = 18
    MUTE_BUTTON_WIDTH = 36
    TOP_DRAW_OFFSET = 8
    PEAK_LABEL_HEIGHT = 20
    PEAK_LABEL_WIDTH = int(round(HORIZONTAL_GRID / 2))


    def __init__(self, parent=None, mixer=None, newlyCreated:bool=False):
        super().__init__(parent=parent)
        self._mixer = mixer
        self._cached_scrollwidget_height = 100
        self._cached_scrollwidget_width = 100
        self._oldCursorPos = None
        self._oldCursorPosTimer = None
        self._newlyCreated = newlyCreated
        self._canSetOutputChannels = False
        self._canSetSinkName = False
        self._hasPeakLabels = False
        # restore window geometry
        self._readIniSettings()
        # setup input widgets
        self._dials = []
        self._invertButtons = []
        self._labels = []
        self._muteButtons = []
        self._spinBoxes = []
        self._zeroButtons = []
        self._peakLabels = []
        # intialize update flags
        self._dialUpdates = True
        self._spinBoxUpdates = True


    def closeEvent(self, e: QEvent):
        self._mixer.setNumInputs(self._spinBoxInputs.value())
        self._writeIniSettings()
        self.parent().repaint()


    def onAdd(self):
        if self._checkAndSaveSinkName() != 0:
            return
        self.parent().xoverFileAddObject(self._mixer)
        self._mixer.restoreWidget(self.parent().drawArea())
        self._writeIniSettings()
        self.close()


    def onCancel(self):
        self._writeIniSettings()
        self.close()


    def onClose(self):
        if self._checkAndSaveSinkName() != 0:
            return
        self._writeIniSettings()
        self._mixer.repaintWidget()
        self.close()


    def showEvent(self, e: QEvent):
        """
        this is a big hack to make stylesheets apply by setting the cursor inside the dialog window and then
        using a singleshot timer with 1ms interval to reset it back to its old position
        """
        # set cursor position inside mixer dialog window
        cursor = self.cursor()
        self._oldCursorPos = cursor.pos()
        newPos = QPoint(self.mapToGlobal(QPoint(20, 80)))
        cursor.setPos(newPos)
        self.setCursor(cursor)
        # setup timer to restore old cursor pos
        self._oldCursorPosTimer = QTimer()
        self._oldCursorPosTimer.setSingleShot(True)
        self._oldCursorPosTimer.setInterval(1)
        # noinspection PyUnresolvedReferences
        self._oldCursorPosTimer.timeout.connect(self._restoreOldCursorPos)
        self._oldCursorPosTimer.start()


    def resizeEvent(self, e=None):
        self.scrollArea.setGeometry(self.LEFTMARGIN,
                                    self.TOPMARGIN,
                                    self.width() - 2 * self.LEFTMARGIN,
                                    self.height() - 3 * self.TOPMARGIN - self.BUTTONS_HEIGHT)
        self.scrollAreaWidgetContents.setFixedHeight(self._cached_scrollwidget_height)
        self.scrollAreaWidgetContents.setFixedWidth(self._cached_scrollwidget_width)
        y = self.height() - self.TOPMARGIN - self.BUTTONS_HEIGHT
        for button in (self._closeButton, self._addButton, self._cancelButton):
            if button is not None:
                button.setGeometry(button.x(), y, button.width(), button.height())


    def _checkAndSaveSinkName(self):
        """ must be reimplemented by subclasses that really need input sink name checking """
        return 0


    def _dialChanged(self, _):
        if not self._dialUpdates:
            return
        dial = self.sender()
        self._mixer.markDirty()
        self._dialUpdates = False
        dial.spinBox.setValue(dial.value() / 10.0)
        self._mixer.gainMatrix()[dial.input_idx][dial.output_idx] = dial.spinBox.value()
        self._dialUpdates = True


    def _invertButtonChanged(self, _):
        button = self.sender()
        self._mixer.markDirty()
        self._mixer.invertMatrix()[button.input_idx][button.output_idx] = button.isChecked()
        self._invertButtonSetStylesheet(button)


    @staticmethod
    def _invertButtonSetStylesheet(button):
        if button.isChecked():
            button.setStyleSheet("background-color: #bb9900; color: #000000; font-size: 11px; font-weight: bold; padding: 2px;")
        else:
            button.setStyleSheet("background-color: #666666; color: #ffffff; font-size: 11px; font-weight: bold; padding: 2px;")


    def _muteButtonChanged(self, _):
        button = self.sender()
        self._mixer.markDirty()
        self._mixer.muteMatrix()[button.input_idx][button.output_idx] = button.isChecked()
        self._muteButtonSetStylesheet(button)


    @staticmethod
    def _muteButtonSetStylesheet(button):
        if button.isChecked():
            button.setStyleSheet("background-color: #ff0000; color: #ffffff; font-size: 11px; font-weight: bold; padding: 2px;")
            button.dial.setStyleSheet("background-color: #000000;")
            button.dial.setEnabled(False)
            button.spinBox.setEnabled(False)
        else:
            button.setStyleSheet("background-color: #666666; color: #ffffff; font-size: 11px; font-weight: bold; padding: 2px;")
            button.dial.setStyleSheet("background-color: %s" % colorForChannelNumRgbString(button.dial.input_idx))
            button.dial.setEnabled(True)
            button.spinBox.setEnabled(True)


    def _onInputChannelsChanged(self):
        self._mixer.setNumInputs(self._spinBoxInputs.value(), doNotAdjustInputs=True)
        self._setupChannelWidgets(newlyCreated=False, reInit=True)


    def _onOutputChannelsChanged(self):
        pass


    def _readIniSettingsFor(self, widgetName:str):
        s = self.parent().settings()
        try:
            x, y, w, h = int(s.value("%s/windowX" % widgetName, None)), \
                         int(s.value("%s/windowY" % widgetName, None)), \
                         int(s.value("%s/windowW" % widgetName, None)), \
                         int(s.value("%s/windowH" % widgetName, None))
        except (TypeError, ValueError):
            x, y, w, h = (None, None, None, None)
        if None not in (x, y, w, h):
            self.setGeometry(x, y, w, h)


    def _restoreOldCursorPos(self):
        cursor = self.cursor()
        cursor.setPos(self._oldCursorPos)
        self.setCursor(cursor)


    def _setupChannelWidgets(self, newlyCreated:bool=False, reInit:bool=False):
        # clear old widgets
        for widgets in (self._dials, self._invertButtons, self._labels, self._muteButtons, self._spinBoxes, self._zeroButtons, self._peakLabels):
            for widget in widgets:
                widget.hide()
                widget.deleteLater()

        # reinit empty widget lists
        self._dials = []
        self._invertButtons = []
        self._spinBoxes = []
        self._labels = []
        self._muteButtons = []
        self._zeroButtons = []
        self._peakLabels = []

        # cache variables
        numInputs = self._mixer.numInputs()
        numOutputs = self._mixer.numOutputs()
        mixer = self._mixer

        # begin drawing
        y = self.TOP_DRAW_OFFSET

        # sink name label
        if self._canSetSinkName:
            if not reInit:
                label = QLabel("Sink name:", self.scrollAreaWidgetContents)
                label.setGeometry(self.LEFTMARGIN, y, 80, self.SPINBOX_HEIGHT)
                # sink name line edit
                self._lineEditSinkName = QLineEdit(mixer.sinkName(), self.scrollAreaWidgetContents)
                self._lineEditSinkName.setGeometry(90, y, 287, self.SPINBOX_HEIGHT)
                self._lineEditSinkName.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
            y += self.SPINBOX_HEIGHT + self.TOPMARGIN

        # input/output channel labels and spin boxes
        if not reInit:
            label = QLabel(self._canSetOutputChannels and "Inputs:" or "Channels:", self.scrollAreaWidgetContents)
            label.setGeometry(self.LEFTMARGIN, y, 80, self.SPINBOX_HEIGHT)
            self._spinBoxInputs = QSpinBox(self.scrollAreaWidgetContents)
            self._spinBoxInputs.setMinimum(MIN_CHANNELS)
            self._spinBoxInputs.setMaximum(MAX_CHANNELS)
            self._spinBoxInputs.setGeometry(90, y, 80, self.SPINBOX_HEIGHT)
            self._spinBoxInputs.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
            self._spinBoxInputs.setValue(numInputs)
            # noinspection PyUnresolvedReferences
            self._spinBoxInputs.valueChanged.connect(self._onInputChannelsChanged)

            if self._canSetOutputChannels:
                label = QLabel("Outputs:", self.scrollAreaWidgetContents)
                label.setGeometry(200, y, 80, self.SPINBOX_HEIGHT)
                self._spinBoxOutputs = QSpinBox(self.scrollAreaWidgetContents)
                self._spinBoxOutputs.setMinimum(MIN_CHANNELS)
                self._spinBoxOutputs.setMaximum(MAX_CHANNELS)
                self._spinBoxOutputs.setGeometry(290, y, 80, self.SPINBOX_HEIGHT)
                self._spinBoxOutputs.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
                self._spinBoxOutputs.setValue(numOutputs)
                # noinspection PyUnresolvedReferences
                self._spinBoxOutputs.valueChanged.connect(self._onOutputChannelsChanged)
            # line
            label = QLabel("", self.scrollAreaWidgetContents)
            label.setGeometry(0, y+33, 2000, 1)
            label.setStyleSheet("background-color: #000000")

        y += self.SPINBOX_HEIGHT + 2 * self.TOPMARGIN
        y_after_label = y + self.SPINBOX_HEIGHT

        # setup labels for input numbers (top row)
        if self._canSetOutputChannels:
            label = QLabel(" Output", self.scrollAreaWidgetContents)
            label.setGeometry(self.LEFTMARGIN, y, 100, self.SPINBOX_HEIGHT)
            label.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
            label.show()
            self._labels.append(label)
            x = self.INPUT_LABEL_WIDTH
        else:
            x = self.LEFTMARGIN
        for i in range(0, numInputs):
            label = QLabel((self._canSetOutputChannels and "Input %d" or "Channel %d") % (i + 1), self.scrollAreaWidgetContents)
            label.setGeometry(x, y, self.HORIZONTAL_GRID, self.SPINBOX_HEIGHT)
            label.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
            x += self.HORIZONTAL_GRID
            self._labels.append(label)

        # setup labels for output numbers (left column)
        if self._canSetOutputChannels:
            for i in range(0, numOutputs):
                label = QLabel(" %d" % (i + 1), self.scrollAreaWidgetContents)
                label.setGeometry(self.LEFTMARGIN,
                                  y + self.SPINBOX_HEIGHT + self.TOPMARGIN,
                                  self.INPUT_LABEL_WIDTH,
                                  self.VERTICAL_GRID + self.TOPMARGIN - 2 * self.SPINBOX_HEIGHT)
                label.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
                y += self.VERTICAL_GRID
                self._labels.append(label)

        # add labels, spinboxes, mute buttons and dials for all mixer channels
        if self._canSetOutputChannels:
            x = self.INPUT_LABEL_WIDTH
        else:
            x = self.LEFTMARGIN
        for input_idx in range(0, numInputs):
            y = y_after_label
            for output_idx in range(0, numOutputs):
                # add dial
                dial = QDial(self.scrollAreaWidgetContents)
                dial.input_idx = input_idx
                dial.output_idx = output_idx
                dial.setMaximum(200)
                dial.setMinimum(-200)
                dial.setValue(int(round(mixer.gainMatrix()[input_idx][output_idx])))
                dial.setSingleStep(1)
                dial.setPageStep(10)
                dial.setValue(int(round(mixer.gainMatrix()[input_idx][output_idx])))
                dial.setGeometry(x + self.DIAL_LEFT,
                                 y,
                                 self.HORIZONTAL_GRID,
                                 self.VERTICAL_GRID - self.SPINBOX_HEIGHT - self.TOPMARGIN)
                dial.setNotchesVisible(True)
                dial.notchSize()
                dial.setNotchTarget(25)
                dial.setStyleSheet("background-color: %s" % colorForChannelNumRgbString(input_idx))
                self._dials.append(dial)
                y += self.VERTICAL_GRID - self.SPINBOX_HEIGHT - 2 * self.TOPMARGIN

                # add spinbox
                spinBox = QDoubleSpinBox(self.scrollAreaWidgetContents)
                spinBox.input_idx = input_idx
                spinBox.output_idx = output_idx
                spinBox.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
                spinBox.setMaximum(20)
                spinBox.setMinimum(-20)
                spinBox.setSingleStep(0.1)
                spinBox.setDecimals(2)
                spinBox.setSuffix("dB")
                spinBox.setValue(mixer.gainMatrix()[input_idx][output_idx])
                spinBox.setGeometry(x + 1 * self.LEFTMARGIN,
                                    y + self.TOPMARGIN,
                                    self.HORIZONTAL_GRID - 2 * self.LEFTMARGIN,
                                    self.SPINBOX_HEIGHT)
                self._spinBoxes.append(spinBox)

                # add zero button
                zbutton = QToolButton(self.scrollAreaWidgetContents)
                zbutton.input_idx = input_idx
                zbutton.output_idx = output_idx
                zbutton.setText("0")
                zbutton.setGeometry(x + self.LEFTMARGIN,
                                    y - self.MUTE_BUTTON_HEIGHT + 4 - 2 * self.MUTE_BUTTON_HEIGHT,
                                    self.MUTE_BUTTON_HEIGHT,
                                    self.MUTE_BUTTON_HEIGHT)
                zbutton.dial = dial
                zbutton.spinBox = spinBox
                zbutton.setStyleSheet("background-color: #006600; color: #ffffff; font-size: 11px; font-weight: bold; padding: 2px;")
                self._zeroButtons.append(zbutton)

                # add invert button
                ibutton = QToolButton(self.scrollAreaWidgetContents)
                ibutton.input_idx = input_idx
                ibutton.output_idx = output_idx
                ibutton.setText("Inv")
                ibutton.setCheckable(True)
                ibutton.setGeometry(x + self.LEFTMARGIN,
                                    y - self.MUTE_BUTTON_HEIGHT + 4 - self.MUTE_BUTTON_HEIGHT,
                                    self.MUTE_BUTTON_WIDTH - 10,
                                    self.MUTE_BUTTON_HEIGHT)
                ibutton.setChecked(mixer.invertMatrix()[input_idx][output_idx])
                self._invertButtonSetStylesheet(ibutton)
                self._invertButtons.append(ibutton)

                # add mute button
                mbutton = QToolButton(self.scrollAreaWidgetContents)
                mbutton.input_idx = input_idx
                mbutton.output_idx = output_idx
                mbutton.setText("Mute")
                mbutton.setCheckable(True)
                mbutton.setGeometry(x + self.LEFTMARGIN,
                                    y - self.MUTE_BUTTON_HEIGHT + 4,
                                    self.MUTE_BUTTON_WIDTH,
                                    self.MUTE_BUTTON_HEIGHT)
                mbutton.setChecked(mixer.muteMatrix()[input_idx][output_idx])
                mbutton.dial = dial
                mbutton.spinBox = spinBox
                self._muteButtonSetStylesheet(mbutton)
                self._muteButtons.append(mbutton)

                # add peak labels
                if self._hasPeakLabels:
                    label = QLabel("Peak:", self.scrollAreaWidgetContents)
                    label.setStyleSheet("font-size: 11px;")
                    label.setGeometry(x + self.LEFTMARGIN,
                                      y + self.PEAK_LABEL_HEIGHT + 10,
                                      self.PEAK_LABEL_WIDTH,
                                      self.PEAK_LABEL_HEIGHT)
                    label.setAlignment(Qt.AlignHCenter)
                    label.show()
                    self._labels.append(label)
                    label = QLabel("-inf dB", self.scrollAreaWidgetContents)
                    label.setStyleSheet("font-size: 11px; font-weight: bold; color: #00ff00;")
                    label.setGeometry(x + self.LEFTMARGIN + self.PEAK_LABEL_WIDTH,
                                      y + self.PEAK_LABEL_HEIGHT + 10,
                                      self.PEAK_LABEL_WIDTH,
                                      self.PEAK_LABEL_HEIGHT)
                    label.setAlignment(Qt.AlignHCenter)
                    label.show()
                    self._peakLabels.append(label)

                # remember corresponding input widgets
                zbutton.spinBox = spinBox
                dial.spinBox = spinBox
                spinBox.dial = dial

                # setup event handlers
                # noinspection PyUnresolvedReferences
                spinBox.valueChanged.connect(self._spinBoxChanged)
                # noinspection PyUnresolvedReferences
                dial.valueChanged.connect(self._dialChanged)
                # noinspection PyUnresolvedReferences
                zbutton.clicked.connect(self._zeroButtonChanged)
                # noinspection PyUnresolvedReferences
                ibutton.toggled.connect(self._invertButtonChanged)
                # noinspection PyUnresolvedReferences
                mbutton.toggled.connect(self._muteButtonChanged)

                # load value into spinbox and dial
                self._dialUpdates = False
                self._spinBoxUpdates = False
                dial.setValue(int(round(mixer.gainMatrix()[input_idx][output_idx] * 10)))
                spinBox.setValue(mixer.gainMatrix()[input_idx][output_idx])
                self._dialUpdates = True
                self._spinBoxUpdates = True

                # increment y
                y += self.SPINBOX_HEIGHT + 2 * self.TOPMARGIN
            # increment x
            x += self.HORIZONTAL_GRID
        self._cached_scrollwidget_height = y + 10
        self._cached_scrollwidget_width = max((self._canSetOutputChannels and self.INPUT_LABEL_WIDTH or 0) + \
                                              numInputs * self.HORIZONTAL_GRID + 2 * self.LEFTMARGIN, 471)
        # add buttons
        y = self.height() - self.TOPMARGIN - self.BUTTONS_HEIGHT
        if not reInit:
            if newlyCreated:
                self._closeButton = None
                self._cancelButton = QPushButton("Cancel", self)
                self._cancelButton.setGeometry(self.LEFTMARGIN, y, 80, 35)
                # noinspection PyUnresolvedReferences
                self._cancelButton.clicked.connect(self.onCancel)
                self._addButton = QPushButton("Add Filter", self)
                self._addButton.setGeometry(self.LEFTMARGIN * 2 + 80, y, 100, 35)
                # noinspection PyUnresolvedReferences
                self._addButton.clicked.connect(self.onAdd)
                self._addButton.setDefault(True)
                if self._canSetSinkName:
                    # find unique sink name for filter
                    oldname = mixer.sinkName()
                    name = oldname
                    i = 0
                    while self.parent().nameExists(name):
                        i += 1
                        name = oldname + str(i)
                    mixer.setSinkName(name)
                    self._lineEditSinkName.setText(name)
            else:
                self._cancelButton = None
                self._addButton = None
                self._closeButton = QPushButton("Close", self)
                self._closeButton.setGeometry(self.LEFTMARGIN, y, 80, 35)
                # noinspection PyUnresolvedReferences
                self._closeButton.clicked.connect(self.onClose)

        # show new widgets
        for widgets in (self._dials, self._invertButtons, self._labels, self._muteButtons, self._spinBoxes, self._zeroButtons):
            for widget in widgets:
                widget.show()

        self.resizeEvent()
        self.repaint()


    def _spinBoxChanged(self, _):
        if not self._spinBoxUpdates:
            return
        spinBox = self.sender()
        self._mixer.markDirty()
        self._dialUpdates = False
        spinBox.dial.setValue(int(round(spinBox.value() * 10)))
        self._mixer.gainMatrix()[spinBox.input_idx][spinBox.output_idx] = spinBox.value()
        self._dialUpdates = True


    def _writeIniSettingsFor(self, widgetName:str):
        """ write ini settings """
        s = self.parent().settings()
        geometry = self.geometry()
        s.setValue("%s/windowX" % widgetName, geometry.x())
        s.setValue("%s/windowY" % widgetName, geometry.y())
        s.setValue("%s/windowW" % widgetName, geometry.width())
        s.setValue("%s/windowH" % widgetName, geometry.height())


    def _zeroButtonChanged(self, _):
        button = self.sender()
        button.spinBox.setValue(0.0)
        self._mixer.markDirty()
