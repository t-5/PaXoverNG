from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QMessageBox, QWidget

from WindowClasses.AbstractMixingDialog import AbstractMixingDialog
from designer_qt5.Ui_EditOutputDialog import Ui_EditOutputDialog

from DataClasses import XoverOutput
from helpers.constants import SAMPLE_RATES
from helpers.functions import alsacap, gainFactorToDb


class EditOutputDialog(AbstractMixingDialog, Ui_EditOutputDialog):

    TOP_DRAW_OFFSET = 135

    def __init__(self, parent: QWidget, output: XoverOutput, newlyCreated:bool=False):
        super().__init__(parent=parent, mixer=output, newlyCreated=newlyCreated)
        self.setupUi(self)
        self._duringSetup = True
        # set up the dialog's widgets
        self._hasPeakLabels = True
        self._setupChannelWidgets(newlyCreated)
        # newly created?
        if newlyCreated:
            self.setWindowTitle("Add new Output...")
            self._addButton.setText("Add Output")
        else:
            self.setWindowTitle("Edit Output")
        geo = self.parent().settings().value("main/outputDialogGeo")
        if geo is not None:
            self.restoreGeometry(geo)
        # get alsa devices and their capabilities
        self._alsa_cards = alsacap()
        # populate alsa devices combobox
        for card in self._alsa_cards:
            for device in card["devices"].values():
                if device.get("min_channels", None) is None:
                    continue
                self.comboBoxOutputDevice.addItem("[hw:%d,%d] %s, %s (%d..%d channels)" %
                                                  (card["num"], device["num"], card["name"], device["name"],
                                                   device["min_channels"], device["max_channels"]),
                                                  {"card": card["num"], "device": device["num"]})
        self.comboBoxOutputDevice.currentIndexChanged.connect(self._ouputDeviceChanged)
        # set alsa devices combobox from output's device setting, set to first found card if no device matches
        output_device = self._mixer.device()
        output_device_found = False
        idx = -1
        for card in self._alsa_cards:
            for device in card["devices"].values():
                idx += 1
                if output_device == "hw:%d,%d" % (card["num"], device["num"]):
                    self.comboBoxOutputDevice.setCurrentIndex(idx)
                    output_device_found = True
        if not output_device_found:
            self.comboBoxOutputDevice.setCurrentIndex(0)
            self._ouputDeviceChanged(0)
        # setup timer to update peak flags
        self._peakFlagTimer = QTimer()
        self._peakFlagTimer.setInterval(1000)
        # noinspection PyUnresolvedReferences
        self._peakFlagTimer.timeout.connect(self._updatePeakLabels)
        self._peakFlagTimer.start()


    def closeEvent(self, event):
        currentData = self.comboBoxOutputDevice.currentData()
        sr = format_ = channels = errors = ""
        if currentData is None:
            errors += "No output device chosen.\n"
        else:
            device = self._alsa_cards[currentData["card"]]["devices"][currentData["device"]]
            format_ = self.comboBoxOutputSampleFormat.currentText()
            if format_ not in device["formats"]:
                errors += "Chose output device does not support sample format '%s'.\n" % format_
            sr = int(self.comboBoxOutputSampleRate.currentText())
            if sr > device["max_samplerate"] or sr < device["min_samplerate"]:
                errors += "Chose output device does not support sample rate %dHz.\n" % sr
            channels = self._spinBoxInputs.value()
            if channels > device["max_channels"] or channels < device["min_channels"]:
                errors += "Chose output device does not support %d output channels.\n" % channels
        if errors:
            dlg = QMessageBox()
            dlg.setStyleSheet(self.styleSheet())
            dlg.critical(self, "Errors in current output configuration:", errors)
            event.ignore()
            return

        self._mixer.setDevice("hw:%d,%d" % (currentData["card"], currentData["device"]))
        self._mixer.setDeviceDescription(self.comboBoxOutputDevice.currentText())

        self._mixer.setSampleFormat(format_)
        self._mixer.setSampleRate(sr)
        self._mixer.setNumInputs(channels)

        if self._newlyCreated:
            self._output.restoreWidget(self.parent().drawArea())
            self.parent().xoverFileAddObject(self._output)

        self.parent().repaintDrawArea()
        self.parent().settings().setValue("main/outputDialogGeo", self.saveGeometry())
        self.close()

    def _readIniSettings(self):
        super()._readIniSettingsFor("editOutputDialog")


    def _ouputDeviceChanged(self, _):
        self._duringSetup = True
        currentData = self.comboBoxOutputDevice.currentData()
        device = self._alsa_cards[currentData["card"]]["devices"][currentData["device"]]

        self.comboBoxOutputSampleFormat.clear()
        for format_ in device["formats"]:
            self.comboBoxOutputSampleFormat.addItem(format_)

        self.comboBoxOutputSampleRate.clear()
        for sr in SAMPLE_RATES:
            if device["min_samplerate"] <= sr <= device["max_samplerate"]:
                self.comboBoxOutputSampleRate.addItem(str(sr), {"samplerate": sr})

        self._duringSetup = False

        format_found = False
        for idx, format_ in enumerate(device["formats"]):
            if self._mixer.sampleFormat() == format_:
                format_found = True
                self.comboBoxOutputSampleFormat.setCurrentIndex(idx)
        if not format_found:
            self.comboBoxOutputSampleFormat.setCurrentIndex(0)

        sr_found = False
        for idx, sr in enumerate(SAMPLE_RATES):
            if sr == self._mixer.sampleRate():
                sr_found = True
                self.comboBoxOutputSampleRate.setCurrentIndex(idx)
        if not sr_found:
            self.comboBoxOutputSampleRate.setCurrentIndex(0)

        self._spinBoxInputs.setMinimum(device["min_channels"])
        self._spinBoxInputs.setMaximum(device["max_channels"])


    def _updatePeakLabels(self):
        peakMatrix = self._mixer.peakMatrix()
        for i in range(self._mixer.numInputs()):
            factor = peakMatrix[i]
            if factor == 0:
                s = "-inf dB"
            else:
                s = "%0.2f dB" % gainFactorToDb(factor)
            try:
                self._peakLabels[i].setText(s)
                if factor >= 1:
                    self._peakLabels[i].setStyleSheet("font-size: 11px; font-weight: bold; color: #ff0000;")
                elif factor >= 0.707:
                    self._peakLabels[i].setStyleSheet("font-size: 11px; font-weight: bold; color: #ffff00;")
                else:
                    self._peakLabels[i].setStyleSheet("font-size: 11px; font-weight: bold; color: #00ff00;")
            except (RuntimeError, IndexError):
                pass


    def _writeIniSettings(self):
        """ write ini settings """
        super()._writeIniSettingsFor("editOutputDialog")
