from PyQt5.QtCore import QEvent, QPoint, QSize
from PyQt5.QtGui import QPainter, QPainterPath, QColor
from PyQt5.QtWidgets import QMainWindow, QWidget
from typing import List

from CustomWidgets.XoverWidget import XoverWidget
from helpers.functions import bezierIntermediatesFor


class DrawArea(QWidget):

    CONNECT_BEZIER_COLOR = QColor(255, 255, 0)

    def __init__(self, parent=None, mainWindow=None):
        super().__init__(parent=parent)
        self._mainWindow = mainWindow
        self.setFixedSize(parent.size())
        self._widgets = []
        self._connectDragPath = None
        self._connectDragStartPos = None
        self._connectDragType = None
        self._connectDragIdx = None


    def addWidget(self, widget: XoverWidget):
        self._widgets.append(widget)
        self.setFixedSize(self.sizeHint())


    def clear(self):
        widgets = self._widgets
        self._widgets = []
        for widget in widgets:
            widget.deleteLater()
        self.setFixedSize(self.sizeHint())


    def connectDragMove(self, dest: QPoint):
        if self._connectDragStartPos is None:
            return
        path = QPainterPath()
        destX = dest.x()
        destY = dest.y()
        im1X, im1Y, im2X, im2Y = bezierIntermediatesFor(self._connectDragStartPos, dest)
        # draw path
        path.moveTo(self._connectDragStartPos.x(), self._connectDragStartPos.y())
        path.cubicTo(im1X, im1Y, im2X, im2Y, destX, destY)
        self._connectDragPath = path
        self.repaint()


    def connectDragStart(self, pos: QPoint, type_: str, idx: int):
        self._connectDragStartPos = pos
        self._connectDragType = type_
        self._connectDragIdx = idx


    def connectDragStop(self, pos: QPoint, fromWidget: XoverWidget):
        found = False
        for toWidget in self._widgets:
            if found:
                break
            if toWidget == fromWidget:
                continue
            if self._connectDragType == "ltr":
                for idx, inputDotRect in enumerate(toWidget.inputDotRectsAbs()):
                    if found:
                        break
                    if inputDotRect.contains(pos):
                        fromWidget.connectWidget(toWidget, self._connectDragIdx, idx)
                        found = True
            if self._connectDragType == "rtl":
                for idx, outputDotRect in enumerate(toWidget.outputDotRectsAbs()):
                    if found:
                        break
                    if outputDotRect.contains(pos):
                        toWidget.connectWidget(fromWidget, idx, self._connectDragIdx)
                        found = True
        self._connectDragStartPos = None
        self._connectDragPath = None
        self._connectDragType = None
        self._connectDragIdx = None
        self.repaint()


    def mainWindow(self) -> QMainWindow:
        return self._mainWindow


    def paintEvent(self, e: QEvent):
        qp = QPainter()
        qp.begin(self)
        qp.setRenderHint(QPainter.Antialiasing)
        for widget in self._widgets:
            for idx, pathTuple in enumerate(widget.connectionPaths()):
                if pathTuple is None:
                    continue
                qp.setPen(pathTuple[1])
                qp.drawPath(pathTuple[0])
        if self._connectDragPath is not None:
            qp.setPen(self.CONNECT_BEZIER_COLOR)
            qp.drawPath(self._connectDragPath)
        qp.end()


    def removeWidget(self, widget: XoverWidget):
        if widget in self._widgets:
            self._widgets.remove(widget)
            self.setFixedSize(self.sizeHint())
            widget.deleteLater()


    def repaint(self):
        self.setFixedSize(self.sizeHint())
        super().repaint()


    def sizeHint(self) -> QSize:
        maxX = 0
        maxY = 0
        for widget in self._widgets:
            widget.resizeToZoomedSize()
            maxX = max(maxX, widget.width() + widget.x())
            maxY = max(maxY, widget.height() + widget.y())
        return QSize(maxX + 2, maxY + 2)


    def widgets(self) -> List[XoverWidget]:
        return self._widgets
