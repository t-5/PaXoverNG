import math
import time

import numpy
from PyQt5 import QtCore
from PyQt5.QtCore import QTimer, QUrl, Qt
from PyQt5.QtGui import QDesktopServices
from PyQt5.QtWidgets import QDialog, QSizePolicy
from designer_qt5.Ui_GlobalFrdDialog import Ui_GlobalFrdDialog
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from qt5_t5darkstyle import darkstyle_css

from CustomWidgets.QCheckList import QCheckList
from helpers.constants import GRAPH_FREQUS
from helpers.functions import GRAPH_SR, interpolateWeightedLinear, keepPhaseBounded, setupPltFigure


class GlobalFrdDialog(QDialog, Ui_GlobalFrdDialog):

    FRD_COLORS = {
        0: "#ff00ff",
        1: "#ffff00",
        2: "#00ff00",
        3: "#9999ff",
    }

    def __init__(self, mainWindow):
        super(GlobalFrdDialog, self).__init__()
        self.setupUi(self)
        self.setStyleSheet(darkstyle_css())
        self._frdPltLines = []
        self._frequencies = numpy.asarray(GRAPH_FREQUS) / math.pi / 2 * GRAPH_SR[0]
        self._graphUpdateInProgress = False
        self._lastGraphUpdate = 0
        self._mainWindow = mainWindow
        self._mplCanvas = None
        self._mplFigure = None
        self._mplCanvasIsSetup = False
        self._nodata = numpy.full(len(GRAPH_FREQUS), -1000000.0)
        self._previousFrdDialogList = []
        self._summedComplexes = numpy.ones(len(GRAPH_FREQUS))
        # settings
        self._settings = mainWindow.settings()
        self._readIniSettings()
        self.verticalLayout.insertWidget(1, self._mplCanvas)
        # QCheckList for shown graph curves
        self._checkListShowCurves = QCheckList()
        self._checkListShowCurves.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self._checkListShowCurves.setMinimumWidth(250)
        self.horizontalLayout_2.removeWidget(self.comboBoxFrdCurves)
        self.comboBoxFrdCurves.hide()
        self.comboBoxFrdCurves.deleteLater()
        self.horizontalLayout_2.insertWidget(7, self._checkListShowCurves)
        # connect actions
        self.checkBoxShowSummedFrequencyResponse.stateChanged.connect(self.reload)
        self.checkBoxShowSummedPhaseResponse.stateChanged.connect(self.reload)
        self.toolButtonClose.clicked.connect(self.onClose)
        self.toolButtonHelp.clicked.connect(self.onHelp)
        # setup update ui timer
        self._timerUpdateUi = QTimer(self)
        self._timerUpdateUi.setInterval(100)
        # noinspection PyUnresolvedReferences
        self._timerUpdateUi.timeout.connect(self._updateGraph)
        self._timerUpdateUi.start()
        self._updateGraph()
        self._lastGraphUpdate = time.time()
        self.setWindowFlags(QtCore.Qt.Window |
                            QtCore.Qt.CustomizeWindowHint |
                            QtCore.Qt.WindowTitleHint |
                            QtCore.Qt.WindowCloseButtonHint |
                            QtCore.Qt.WindowSystemMenuHint)


    def onClose(self):
        self.close()


    def reload(self):
        self._lastGraphUpdate = 0


    @staticmethod
    def onHelp():
        url = QUrl('https://t-5.eu/hp/Software/Pulseaudio%20Crossover%20NG/OnlineHelp/#frd_comparison')
        QDesktopServices().openUrl(url)


    def _frdCurveColor(self, idx: int) -> str:
        return self.FRD_COLORS.get(idx, "#ffffff")


    def _readIniSettings(self):
        s = self._settings
        # dialog geometry and settings
        try:
            x, y, w, h = (int(s.value("globalFrdDialog/x", None)),
                          int(s.value("globalFrdDialog/y", None)),
                          int(s.value("globalFrdDialog/width", None)),
                          int(s.value("globalFrdDialog/height", None)))
        except (TypeError, ValueError):
            x, y, w, h = None, None, None, None
        if None not in (x, y, w, h):
            self.setGeometry(x, y, w, h)
        # checkbox and radiobutton settings
        self.checkBoxShowSummedFrequencyResponse.setChecked(
            s.value("globalFrdDialog/showSummedFrequencyResponse", "true") in (True, "true"))
        self.checkBoxShowSummedPhaseResponse.setChecked(
            s.value("globalFrdDialog/showSummedPhaseResponse", "true") in (True, "true"))


    def _setupMplCanvas(self, w):
        # remove old matplotlib canvas or placeholder from vertical layout
        if self._mplCanvas is None:
            try:
                self.widgetReplacedByFrGraph.hide()
                self.verticalLayout.removeWidget(self.widgetReplacedByFrGraph)
                self.widgetReplacedByFrGraph.deleteLater()
            except RuntimeError:
                pass
        else:
            self._mplCanvas.hide()
            self.verticalLayout.removeWidget(self._mplCanvas)
            self._mplCanvas.deleteLater()
        # setup new matplotlib canvas and figure
        self._mplFigure = Figure()
        self._mplCanvas = FigureCanvas(self._mplFigure)
        self._mplCanvas.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.verticalLayout.insertWidget(1, self._mplCanvas)
        self._pltLine1, self._pltAx1, self._pltLine2, self._pltAx2 = \
            setupPltFigure(self._frequencies, self._nodata,
                           self._mplCanvas, self._mplFigure,
                           -30, 10, -200, 200, phaseLineThickness=1.0)
        self._frdPltLines = []
        for idx, dlg in enumerate(self._mainWindow.frdDialogs()):
            pltLine, = self._pltAx1.plot(w, self._nodata, self._frdCurveColor(idx), linewidth=1.0, zorder=5)
            self._frdPltLines.append(pltLine)
        self._pltLineSummedMagnitude, = self._pltAx1.plot(w, self._nodata, "#00ffff", linewidth=2.0, zorder=10)
        self._mplCanvas.setMinimumHeight(100)
        self._mplFigure.subplots_adjust(left=0.06, right=0.94, top=0.98, bottom=0.07)


    def _updateGraph(self):
        if self._graphUpdateInProgress:
            return
        if self.isHidden():
            return
        # only update if one of the frd dialogs was updated after we last updated
        newestFrdDialogGraphUpdate = 0
        for dlg in self._mainWindow.frdDialogs():
            newestFrdDialogGraphUpdate = max(newestFrdDialogGraphUpdate, dlg.lastGraphUpdate())
        if self._lastGraphUpdate >= newestFrdDialogGraphUpdate:
            return
        self._graphUpdateInProgress = True
        # need to repopulate checkList with frd curves if new dialogs were added
        if set(self._previousFrdDialogList) != set(self._mainWindow.frdDialogs()):
            self._mplCanvasIsSetup = False
            checked = {}
            for idx in range(self._checkListShowCurves.rowCount()):
                item = self._checkListShowCurves.item(idx)
                checked[item.text()] = item.checkState()
            self._checkListShowCurves.clear()
            for dlg in self._mainWindow.frdDialogs():
                xoverFilter = dlg.xoverFilter()
                self._checkListShowCurves.addCheckItem(xoverFilter.sinkName(),
                                                       dlg,
                                                       checked.get(xoverFilter.sinkName(), Qt.Checked))
            self._previousFrdDialogList = list(self._mainWindow.frdDialogs())
        # do interpolation on all magnitude response data sets to have unique frequencies
        # make a union of all frequencies, sort and convert to numpy array
        frequencies = set()
        for dlg in self._mainWindow.frdDialogs():
            frequencies = frequencies.union(dlg.frequencies())
        self._frequencies = numpy.asarray(sorted(list(frequencies)))
        # interpolate all magnitude responses of frd dialogs to new target frequencies
        self._interpolatedComplexes = []
        self._summedComplexes = numpy.full(len(self._frequencies), 0, dtype=numpy.complexfloating)
        for dlg in self._mainWindow.frdDialogs():
            newComplexes = []
            frdFrequencies = dlg.frequencies()
            frdComplexes = dlg.summedComplexes()
            # construct a mapping frdFrequency -> frdComplex for cheap lookup
            frdFrequencyToComplexMapping = {}
            for idx, frdFrequency in enumerate(frdFrequencies):
                frdFrequencyToComplexMapping[frdFrequency] = frdComplexes[idx]
            # step through all target frequencies
            for idx, targetFrequency in enumerate(self._frequencies):
                # only interpolate if there is no immediate value availaple in the mapping
                if targetFrequency in frdFrequencyToComplexMapping:
                    complex_ = frdFrequencyToComplexMapping[targetFrequency]
                else:
                    complex_ = interpolateWeightedLinear(targetFrequency, frdFrequencies, frdComplexes)
                newComplexes.append(complex_)
                self._summedComplexes[idx] = self._summedComplexes[idx] + complex_
            self._interpolatedComplexes.append(numpy.asarray(newComplexes))
        # do initial graph setup?
        if not self._mplCanvasIsSetup:
            # prepare new "nodata" array, as length of frequencies might have changed
            self._nodata = numpy.full(len(self._frequencies), -1000000)
            # setup the new matplotlib canvas
            self._setupMplCanvas(self._frequencies)
            self._mplCanvasIsSetup = True
            # disable line1 for this graph
            self._pltLine1.set_ydata(self._nodata)
        # fill y data, summed magnitude response
        if self.checkBoxShowSummedFrequencyResponse.isChecked():
            self._pltLineSummedMagnitude.set_ydata(20 * numpy.log10(abs(self._summedComplexes)))
        else:
            self._pltLineSummedMagnitude.set_ydata(self._nodata)
        # fill y data, summed phase response
        if self.checkBoxShowSummedPhaseResponse.isChecked():
            self._pltLine2.set_ydata(keepPhaseBounded(numpy.angle(self._summedComplexes) * 180 / math.pi))
        else:
            self._pltLine2.set_ydata(self._nodata)
        # fill y data for frd curves
        for idx, dlg in enumerate(self._mainWindow.frdDialogs()):
            item = self._checkListShowCurves.item(idx)
            if item.checkState():
                self._frdPltLines[idx].set_ydata(20 * numpy.log10(abs(self._interpolatedComplexes[idx])))
            else:
                self._frdPltLines[idx].set_ydata(self._nodata)
        self._lastGraphUpdate = time.time()
        self._mplCanvas.draw()
        self._graphUpdateInProgress = False


    def _writeIniSettings(self):
        s = self._settings
        # dialog geometry and settings
        geometry = self.geometry()
        s.setValue("globalFrdDialog/x", geometry.x())
        s.setValue("globalFrdDialog/y", geometry.y())
        s.setValue("globalFrdDialog/height", geometry.height())
        s.setValue("globalFrdDialog/width", geometry.width())
        # checkbox and radiobutton settings
        s.setValue("globalFrdDialog/showSummedFrequencyResponse", self.checkBoxShowSummedFrequencyResponse.isChecked())
        s.setValue("globalFrdDialog/showSummedPhaseResponse", self.checkBoxShowSummedPhaseResponse.isChecked())
