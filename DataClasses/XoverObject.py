from typing import List

from PyQt5.QtGui import QColor

from CustomWidgets.XoverWidget import XoverWidget

try:
    from ZODB.utils import u64
    from persistent import Persistent
except ImportError:
    u64 = None
    Persistent = object
    pass # import errors will be handled in MainWindow

from helpers.functions import testingIdGeneratorGet


class XoverObject(Persistent):
    """
    Crossover Object Base Class
    """
    def __init__(self):
        # noinspection PyArgumentList
        Persistent.__init__(self)
        self._widgetX = 10 # type: int
        self._widgetY = 10 # type: int
        self._v_widget = None


    def applyFilterParams(self):
        """ noop, must be reimplemented by real filters """
        pass


    def info(self) -> str:
        raise NotImplementedError


    def hexId(self) -> str:
        return "%x" % self.numId()


    def numId(self) -> int:
        if self._p_oid is None:
            # generate our own ids for testing
            return testingIdGeneratorGet()
        return u64(self._p_oid)


    def outputColors(self) -> List[QColor]:
        ret = getattr(self, "_v_outputColors", None)
        if ret is None:
            ret = self._calculateOutputColors()
        return ret


    def resetOutputColors(self):
        try:
            del self._v_outputColors
        except AttributeError:
            pass


    def setWidget(self, widget):
        self._v_widget = widget


    def setWidgetX(self, x):
        self._widgetX = x


    def setWidgetY(self, y):
        self._widgetY = y


    def tempSinkName(self) -> str:
        """ generate temporary sink name from numId """
        return "PaXoverSink%s" % self.numId()


    def widget(self) -> XoverWidget:
        return self._v_widget


    def widgetX(self) -> int:
        return self._widgetX


    def widgetY(self) -> int:
        return self._widgetY


    def _calculateOutputColors(self):
        raise NotImplementedError("method _calculateOutputColors of class %s called." % self.__class__.__name__)