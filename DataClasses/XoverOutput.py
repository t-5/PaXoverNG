from CustomWidgets.XoverWidget import XoverWidget
from DataClasses.XoverMixingObject import XoverMixingObject
from helpers.constants import GRAPH_FREQUS

try:
    from persistent.list import PersistentList
except ImportError:
    PersistentList = None # import errors will be handled in MainWindow

from CustomWidgets.OutputWidget import OutputWidget


class XoverOutput(XoverMixingObject):
    """
    Crossover Output Class
    """

    def __init__(self, sink_name: str):
        super().__init__(sink_name)
        # number of outputs is fake for the mixing widgets and always will be 1!
        self._numOutputs = 1 # type: int
        self._sink_name = "Output"
        self._device = "" # ALSA output device
        self._deviceDescription = "" # ALSA output device description
        self._sampleRate = 44100 # sample format of the ALSA device
        self._sampleFormat = "S16_LE" # sample rate to run camilladsp at
        self._v_widget = None # volatile instance variable for holding the qt widget
        self._peakMatrix = []
        self._initPeakMatrix()


    def __repr__(self):
        return "<%s sink_name='%s' />" % (self.__class__.__name__, self._sink_name)


    def camillaDspSnippet(self):
        numInputs = len(self._inputs)
        ret = "  %s\n    channels:\n      in: %d\n      out: %d\n    mapping:\n" % \
              (self.sinkName(), numInputs, numInputs)
        for inputIdx in range(numInputs):
            ret += "      - dest: %d\n        sources:\n" % inputIdx
            ret += "          - channel: %d\n            gain: %f\n            mute: %s\n            inverted: %s\n" % \
                   (inputIdx,
                    self._gainMatrix[inputIdx][0],
                    self._muteMatrix[inputIdx][0] and "true" or "false",
                    self._invertMatrix[inputIdx][0] and "true" or "false")
        return ret



    def chunkSize(self):
        # TODO: make configurable
        return {
            44100: 1024,
            48000: 1024,
            88200: 2048,
            96000: 2048,
            176400: 4096,
            192000: 4096,
            352800: 8192,
            384000: 8192,
            705600: 8192,
            768000: 8192,
        }[self._sampleRate]


    def device(self):
        return self._device


    def deviceDescription(self):
        return self._deviceDescription


    @staticmethod
    def filterName() -> str:
        raise NotImplementedError


    def frequencyResponse(self, frequencies=GRAPH_FREQUS):
        raise NotImplementedError


    def info(self) -> str:
        return ""


    def inputs(self) -> PersistentList:
        return self._inputs


    def markDirty(self):
        pass


    def numOutputsForDrawing(self):
        return 0


    def outputs(self) -> PersistentList:
        return PersistentList([])


    def peakMatrix(self):
        return self._peakMatrix


    def restoreWidget(self, drawArea):
        """ restore widget for all connected xover objects """
        self._v_widget = OutputWidget(drawArea, self, self._widgetX, self._widgetY)
        self._v_widget.show()
        
        
    def sampleFormat(self):
        return self._sampleFormat


    def sampleRate(self):
        return self._sampleRate


    def setDevice(self, device):
        self._device = device


    def setDeviceDescription(self, deviceDescription):
        self._deviceDescription = deviceDescription


    def setNumInputs(self, value, doNotAdjustInputs=False):
        super().setNumInputs(value, doNotAdjustInputs)
        self._initPeakMatrix()


    def setPeakMatrix(self, peakMatrix):
        self._peakMatrix = peakMatrix


    def setSampleFormat(self, sampleFormat):
        self._sampleFormat = sampleFormat


    def setSampleRate(self, sampleRate):
        self._sampleRate = sampleRate


    def setSinkName(self, sink_name):
        self._sink_name = sink_name


    def sinkName(self):
        return self._sink_name


    def widget(self) -> XoverWidget:
        return self._v_widget


    def _initPeakMatrix(self):
        self._peakMatrix = [0] * self._numInputs
