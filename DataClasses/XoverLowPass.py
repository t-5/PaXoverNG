from DataClasses.XoverLowHighPass import XoverLowHighPass


class XoverLowPass(XoverLowHighPass):
    """
    Crossover Low Pass Filter Base Class
    """

    def __init__(self, sink_name="", frequency=1000):
        super().__init__(sink_name, frequency)

        
    def camillaDspSnippet(self):
        raise NotImplementedError


    @staticmethod
    def filterName() -> str:
        pass


    @staticmethod
    def frequencyResponseCalculations():
        raise NotImplementedError


    def _coeffsBA(self):
        raise NotImplementedError
