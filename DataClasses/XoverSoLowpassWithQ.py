from math import cos, pi, sin

from DataClasses.XoverLowHighPassWithQ import XoverLowHighPassWithQ
from helpers.functions import GRAPH_SR, prettyFrequency


class XoverSoLowpassWithQ(XoverLowHighPassWithQ):
    """
    Second Order Low-Pass Filter With Q
    """
    def __init__(self, sink_name:str="", fequency:float=1000, q:float=1):
        super().__init__(sink_name, fequency, q)


    def camillaDspSnippet(self):
        return "  %s:\n    type: Lowpass\n    freq: %f\n    q: %f\n" % \
               (self.sinkName(), self._frequency, self._q)


    @staticmethod
    def filterName() -> str:
        return "Second Order Lowpass with Q"


    @staticmethod
    def frequencyResponseCalculations() -> int:
        return 1


    def info(self) -> str:
        return "Cutoff: %s | Q: %0.2f" % (prettyFrequency(self._frequency), self._q)


    def _coeffsBA(self):
        """ return 12db/oct low pass biquad coeffs for given frequency """
        w0 = 2 * pi * self._frequency / GRAPH_SR[0]
        alpha = sin(w0) / 2 / self._q
        cs = cos(w0)
        norm = 1 / (1 + alpha)
        b0 = (1 - cs) / 2 * norm
        b1 = (1 - cs) * norm
        b2 = b0
        a1 = -2 * cs * norm
        a2 = (1 - alpha) * norm
        return (b0, b1, b2), (1, a1, a2)
