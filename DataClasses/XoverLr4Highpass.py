from math import cos, sin, pi

from DataClasses.XoverHighPass import XoverHighPass
from helpers.functions import GRAPH_SR


class XoverLr4Highpass(XoverHighPass):
    """
    Crossover LR-4 High-Pass Filter Class
    """
    def __init(self, sink_name:str="", fequency:float=1000):
        super().__init__(sink_name, fequency)


    def camillaDspSnippet(self):
        return "  %s:\n    type: LinkwitzRileyHighpass\n    freq: %f\n    order: 4\n" % \
               (self.sinkName(), self._frequency)


    @staticmethod
    def filterName() -> str:
        return "LR-4 High Pass"


    @staticmethod
    def frequencyResponseCalculations() -> int:
        return 2


    def _coeffsBA(self):
        """ return 12db/oct low pass biquad coeffs for given frequency """
        w0 = 2 * pi * self._frequency / GRAPH_SR[0]
        alpha = sin(w0) / 2 / 0.7071067811865476 # Butterworth characteristic, Q = 0.707...
        cs = cos(w0)
        norm = 1 / (1 + alpha)
        b0 = (1 + cs) / 2 * norm
        b1 = -1.0 * (1 + cs) * norm
        b2 = b0
        a1 = -2 * cs * norm
        a2 = (1 - alpha) * norm
        return (b0, b1, b2), (1, a1, a2)
