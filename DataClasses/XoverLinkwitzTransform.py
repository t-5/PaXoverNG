from math import pi, tan

from scipy import signal

from DataClasses.XoverFilter import XoverFilter
from helpers.constants import GRAPH_FREQUS
from helpers.functions import GRAPH_SR, prettyFrequency


class XoverLinkwitzTransform(XoverFilter):
    """
    Second Order Low-Pass Filter With Q
    """
    def __init__(self, sink_name="", frequency=1000, q=0.7071067, f0=1000, q0=0.7071067):
        super().__init__(sink_name)
        self._frequency = frequency
        self._q = q # type: float
        self._f0 = f0 # type: float
        self._q0 = q0 # type: float
        self._linkedBy = None


    def camillaDspSnippet(self):
        return "  %s:\n    type: LinkwitzTransform\n    freq_target: %f\n    q_target: %f\n    freq_act: %f\n    q_act: %f\n" % \
               (self.sinkName(), self._frequency, self._q, self._f0, self._q0)


    @staticmethod
    def canBeLinked() -> bool:
        return True


    def disconnectAll(self):
        super().disconnectAll()
        if self._linkedBy is not None:
            self._linkedBy.removeLinkedFilter(self)


    @staticmethod
    def filterName() -> str:
        return "Linkwitz Transform"


    def frequency(self) -> float:
        return self._frequency


    def f0(self) -> float:
        return self._f0


    def frequencyResponse(self, frequencies=GRAPH_FREQUS):
        """ return w, h frequency response components for filter """
        b, a = self._coeffsBA()
        return signal.freqz(b, a, worN=frequencies, whole=True)


    @staticmethod
    def gainGraphLowerLimit() -> int:
        return -30


    @staticmethod
    def gainGraphUpperLimit() -> int:
        return 20


    def info(self) -> str:
        return "F: %s|Q: %0.2f|F0: %s|Q0: %0.2f" % (prettyFrequency(self._frequency), self._q,
                                                    prettyFrequency(self._f0), self._q0)


    def linkedBy(self):
        return self._linkedBy


    def parameterMappings(self) -> list:
        if getattr(self, "_v_cached_parameterMappings", None) is None:
            ret = super().parameterMappings()[:]
            ret.append({
                "description": "Target Frequency [Hz]",
                "getter": self.frequency,
                "setter": self.setFrequency,
                "min": 2,
                "default": 1000,
                "max": 20000,
                "scale": "log2",
            })
            ret.append({
                "description": "Target Q",
                "getter": self.q,
                "setter": self.setQ,
                "min": 0.1,
                "default": 0.7071067,
                "max": 10,
                "scale": "log10",
            })
            ret.append({
                "description": "Original Frequency [Hz]",
                "getter": self.f0,
                "setter": self.setF0,
                "min": 2,
                "default": 1000,
                "max": 20000,
                "scale": "log2",
            })
            ret.append({
                "description": "Original Q",
                "getter": self.q0,
                "setter": self.setQ0,
                "min": 0.1,
                "default": 0.7071067,
                "max": 10,
                "scale": "log10",
            })
            self._v_cached_parameterMappings = ret
        return self._v_cached_parameterMappings


    def q(self) -> float:
        return self._q


    def q0(self) -> float:
        return self._q0


    def setFrequency(self, frequency):
        self._frequency = frequency
        self._v_dirty = True
        if self._linkedBy is not None:
            self._linkedBy.broadCastFrequencyExceptTo(self, frequency)


    def setFrequencyNoLinkUpdate(self, frequency):
        self._frequency = frequency
        self._v_dirty = True


    def setF0(self, v):
        self._f0 = v


    def setLinkedBy(self, linkedBy):
        self._linkedBy = linkedBy
        self._v_dirty = True


    def setQ(self, v):
        self._q = v


    def setQ0(self, v):
        self._q0 = v


    def _coeffsBA(self):
        """ return linkwitz transform coefficients """
        d0i = pow(2 * pi * self._f0, 2)
        d1i = (2 * pi * self._f0) / self._q0
        d2i = 1
        c0i = pow(2 * pi * self._frequency, 2)
        c1i = (2 * pi * self._frequency) / self._q
        c2i = 1
        gn = 2 * pi / (tan(pi / GRAPH_SR[0]))
        cci = c0i + gn * c1i + gn * gn * c2i

        a1 = (2 * (c0i - gn * gn * c2i) / cci)
        a2 = ((c0i - gn * c1i + gn * gn * c2i) / cci)
        b0 = (d0i + gn * d1i + gn * gn * d2i) / cci
        b1 = 2 * (d0i - (gn * gn * d2i)) / cci
        b2 = (d0i - gn * d1i + gn * gn * d2i) / cci

        return (b0, b1, b2), (1, a1, a2)
