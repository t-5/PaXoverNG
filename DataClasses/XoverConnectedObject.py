
from PyQt5.QtGui import QColor

from DataClasses.XoverConnection import XoverConnection

try:
    from persistent.list import PersistentList
except ImportError:
    PersistentList = None # import errors will be handled in MainWindow

from DataClasses.XoverObject import XoverObject

class XoverConnectedObject(XoverObject):
    """
    Crossover Connected Object Base Class
    """

    def __init__(self):
        super().__init__()
        self._outputs = PersistentList([None]) # type: PersistentList
        self._inputs = PersistentList([None]) # type: PersistentList
        self._v_fakeInputs = []
        self._v_fakeOutputs = []


    def addChannel(self):
        self._inputs.append(None)
        self._outputs.append(None)


    def allMixers(self):
        """ return dictionary of id => XoverMixer pairs """
        ret = {}
        for output in self._outputs:
            if output is None:
                continue
            if output.toObject().isMixer():
                ret[id(output.toObject())] = output.toObject()
            ret = {**ret, **output.toObject().allMixers()}
        return ret


    def connect(self, idx: int, filter_, otherIdx: int):
        """ must be called on the XoverConnectedObject that owns the output side """
        if self._outputs[idx] is not None:
            return
        if filter_.inputs()[otherIdx] is not None:
            return
        connection = XoverConnection(self, idx, filter_, otherIdx)
        self._outputs[idx] = connection
        filter_.connectReverse(connection)


    def connectReverse(self, connection: XoverConnection):
        """ must be called on the XoverConnectedObject that owns the input side """
        if self._inputs[connection.toIndex()] is not None:
            return
        self._inputs[connection.toIndex()] = connection


    def disconnectAll(self):
        """ disconnect all connections and links """
        for connection in list(self._outputs[:]):
            if connection is not None:
                self.disconnect(connection)
        for connection in list(self._inputs)[:]:
            if connection is not None:
                connection.fromObject().disconnect(connection)


    def disconnect(self, connection: XoverConnection):
        """ must be called on the XoverConnectedObject that owns the output side """
        if self._outputs[connection.fromIndex()] is None:
            return
        connection.toObject().disconnectReverse(connection)
        self._outputs[connection.fromIndex()] = None


    def disconnectReverse(self, connection: XoverConnection):
        """ must be called on the XoverConnectedObject that owns the input side """
        self._inputs[connection.toIndex()] = None


    def fakeInputs(self):
        return self._v_fakeInputs


    def fakeOutputs(self):
        return self._v_fakeOutputs


    def hasInputConnectionAt(self, idx: int) -> bool:
        try:
            return self._inputs[idx] is not None
        except IndexError:
            pass


    def hasOutputConnections(self) -> bool:
        return len(list(filter(lambda x: x is not None, self._outputs))) > 0


    def hasOutputConnectionAt(self, idx) -> bool:
        return self._outputs[idx] is not None


    def identicalFakeInputs(self):
        """ set fake inputs to same as inputs """
        self._v_fakeInputs = list(self._inputs)


    def identicalFakeOutputs(self):
        """ set fake outputs to same as outputs """
        self._v_fakeOutputs = list(self._outputs)


    def info(self):
        raise NotImplementedError


    def initFakeChannelMappings(self, _):
        for output in self._outputs:
            output.initFakeChannelMappings(self._outputs)


    def inputs(self) -> PersistentList:
        return self._inputs


    def isConnectedToInputOf(self, xoverObject):
        for connection in self._outputs:
            if connection is None:
                continue
            if xoverObject == connection.toObject():
                return True
            result = connection.toObject().isConnectedToInputOf(xoverObject)
            if result:
                return True
        return False


    def isConnectedToOutputOf(self, xoverObject):
        for connection in self._inputs:
            if connection is None:
                continue
            if xoverObject == connection.fromObject():
                return True
            result = connection.fromObject().isConnectedToOutputOf(xoverObject)
            if result:
                return True
        return False


    @staticmethod
    def isMixer():
        return False


    def nextFakeInputIdx(self):
        ret = self._v_nextFakeInputIdx
        self._v_nextFakeInputIdx += 1
        return ret


    def nextFakeOutputIdx(self):
        ret = self._v_nextFakeOutputIdx
        self._v_nextFakeOutputIdx += 1
        return ret


    def numInputs(self) -> int:
        return len(self._inputs)


    def numOutputs(self) -> int:
        return len(self._outputs)


    def numOutputsForDrawing(self):
        return self.numOutputs()


    def outputs(self) -> PersistentList:
        return self._outputs


    def outputSinkNamesText(self) -> str:
        return ",".join(map(lambda x: x.toObject().sinkName(), self._outputs))


    def removeChannel(self):
        if self._inputs[-1] is not None or self._outputs[-1] is not None:
            return
        del self._inputs[-1]
        del self._outputs[-1]


    def resetFakeChannels(self):
        self._v_fakeInputs = []
        self._v_fakeOutputs = []


    def upstreamMixers(self) -> list:
        ret = {}
        for input_ in self._inputs:
            for mixer in input_.upstreamMixers():
                ret[id(mixer)] = mixer
        return list(ret.values())


    def _calculateOutputColors(self):
        ret = []
        for idx in range(0, len(self._outputs)):
            input_ = self._inputs[idx]
            if input_ is None:
                ret.append(QColor(0, 0, 0))
            else:
                ret.append(input_.fromObject().outputColors()[input_.fromIndex()])
        return ret
