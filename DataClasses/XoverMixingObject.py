from persistent.list import PersistentList

from DataClasses.XoverFilter import XoverFilter
from helpers.constants import GRAPH_FREQUS


class XoverMixingObject(XoverFilter):

    def __init__(self, sink_name=""):
        super().__init__(sink_name)
        self._numInputs = 2 # type: int
        self._numOutputs = 2 # type: int
        self._inputs = PersistentList([None, None]) # type: PersistentList
        self._gainMatrix = PersistentList() # type: PersistentList
        self._invertMatrix = PersistentList() # type: PersistentList
        self._muteMatrix = PersistentList() # type: PersistentList
        self._initGainMatrix()
        self._initInvertMatrix()
        self._initMuteMatrix()


    def info(self) -> str:
        raise NotImplementedError


    def camillaDspSnippet(self):
        raise NotImplementedError


    @staticmethod
    def filterName() -> str:
        raise NotImplementedError


    def frequencyResponse(self, frequencies=GRAPH_FREQUS):
        raise NotImplementedError


    def gainMatrix(self) -> PersistentList:
        return self._gainMatrix


    def invertMatrix(self) -> PersistentList:
        return self._invertMatrix


    def muteMatrix(self) -> PersistentList:
        return self._muteMatrix


    def numInputs(self) -> int:
        return self._numInputs


    def numOutputs(self) -> int:
        return self._numOutputs


    def setNumInputs(self, value, doNotAdjustInputs=False):
        self._numInputs = value
        if not doNotAdjustInputs:
            self._adjustInputs()
        self._initGainMatrix()
        self._initInvertMatrix()
        self._initMuteMatrix()


    def _adjustInputs(self):
        if len(self._inputs) > self._numInputs:
            for input_ in list(self._inputs[self._numInputs:]):
                if input_ is not None:
                    input_.disconnect()
            for _ in list(self._inputs[self._numInputs:]):
                del self._inputs[-1]
        while len(self._inputs) < self._numInputs:
            self._inputs.append(None)


    def _adjustOutputs(self):
        if len(self._outputs) > self._numOutputs:
            for output in list(self._outputs[self._numOutputs:]):
                if output is not None:
                    output.disconnect()
            for _ in list(self._outputs[self._numOutputs:]):
                del self._outputs[-1]
        while len(self._outputs) < self._numOutputs:
            self._outputs.append(None)


    def _initGainMatrix(self):
        newGainMatrix = []
        for inputIdx in range(0, self._numInputs):
            try:
                newGainMatrix.append(PersistentList(self._gainMatrix[inputIdx][:self._numOutputs]))
            except IndexError:
                newGainMatrix.append(PersistentList())
            while len(newGainMatrix[inputIdx]) < self._numOutputs:
                newGainMatrix[inputIdx].append(0.0)
        self._gainMatrix = newGainMatrix


    def _initInvertMatrix(self):
        newInvertMatrix = []
        for inputIdx in range(0, self._numInputs):
            try:
                newInvertMatrix.append(PersistentList(self._invertMatrix[inputIdx][:self._numOutputs]))
            except IndexError:
                newInvertMatrix.append(PersistentList())
            while len(newInvertMatrix[inputIdx]) < self._numOutputs:
                newInvertMatrix[inputIdx].append(False)
        self._invertMatrix = newInvertMatrix


    def _initMuteMatrix(self):
        newMuteMatrix = []
        for inputIdx in range(0, self._numInputs):
            try:
                newMuteMatrix.append(PersistentList(self._muteMatrix[inputIdx][:self._numOutputs]))
            except IndexError:
                newMuteMatrix.append(PersistentList())
            while len(newMuteMatrix[inputIdx]) < self._numOutputs:
                newMuteMatrix[inputIdx].append(True)
        self._muteMatrix = newMuteMatrix
