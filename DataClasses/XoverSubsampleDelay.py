from DataClasses.XoverDelay import XoverDelay


class XoverSubsampleDelay(XoverDelay):
    """
    Subsample Accurate Delay Filter Class
    """
    def __init(self, sink_name:str="", delay:float=0):
        super().__init__(sink_name, delay=delay)


    def camillaDspSnippet(self):
        return "  %s:\n    type: Delay\n    delay: %f\n    unit: ms\n    subsample: true\n" % \
               (self.sinkName(), self._delay)


    @staticmethod
    def filterName() -> str:
        return "Subsample Accurate Delay"


    def _coeffsBA(self):
        """ return linear frequency response """
        delaySamples = self.delaySamples()
        delaySubSamples = delaySamples - int(delaySamples)
        ## removed for now
        # prepend <delaySamples> zeros to coefficients
        #b = numpy.zeros(int(self.delaySamples()) + 2)
        #a = numpy.zeros(int(self.delaySamples()) + 2)
        #b[-2] = 1 - delaySubSamples
        #b[-1] = delaySubSamples
        #a[-2] = 1
        return (1 - delaySubSamples, delaySubSamples), (1, 0)
