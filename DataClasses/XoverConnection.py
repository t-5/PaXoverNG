
try:
    from ZODB.utils import u64
    from persistent import Persistent
except ImportError:
    u64 = None
    Persistent = object
    pass # import errors will be handled in MainWindow


class XoverConnection(Persistent):
    """
    Crossover Connection Class
    """

    def __init__(self, fromObject, fromIndex, toObject, toIndex, isFake=False):
        # noinspection PyArgumentList
        Persistent.__init__(self)
        self._fromObject = fromObject
        self._fromIndex = fromIndex
        self._toObject = toObject
        self._toIndex = toIndex
        self._isFake = isFake


    def __repr__(self):
        return "<XoverConnection from='%s@%d' to='%s@%d'>" % (self._fromObject.sinkName(),
                                                              self._fromIndex,
                                                              self._toObject.sinkName(),
                                                              self._toIndex)


    def disconnect(self):
        self._fromObject.disconnect(self)


    def fake(self, newToIndex):
        """ return a fake connection object with my input and output """
        return XoverConnection(self._fromObject, self._fromIndex, self._toObject, newToIndex, True)


    def fromObject(self):
        return self._fromObject
    
    
    def fromIndex(self):
        return self._fromIndex


    def isFake(self):
        # TODO: remove
        try:
            return self._isFake
        except AttributeError:
            self._isFake = False
            return self._isFake


    def isIdenticalTo(self, connection):
        # TODO: remove
        if self._isFake != connection.isFake():
            return False
        if self._toIndex != connection.toIndex():
            return False
        if self._toObject != connection.toObject():
            return False
        if self._fromIndex != connection.fromIndex():
            return False
        if self._fromObject != connection.fromObject():
            return False
        return True
    
    
    def toObject(self):
        return self._toObject
    
    
    def toIndex(self):
        return self._toIndex
