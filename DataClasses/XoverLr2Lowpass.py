from math import pi, tan

from DataClasses.XoverLowPass import XoverLowPass
from helpers.functions import GRAPH_SR


class XoverLr2Lowpass(XoverLowPass):
    """
    Crossover LR-2 Low-Pass Filter Class
    """
    def __init(self, sink_name:str="", fequency:float=1000):
        super().__init__(sink_name, fequency)


    def camillaDspSnippet(self):
        return "  %s:\n    type: LinkwitzRileyLowpass\n    freq: %f\n    order: 2\n" % \
               (self.sinkName(), self._frequency)


    @staticmethod
    def filterName() -> str:
        return "LR-2 Low Pass"


    @staticmethod
    def frequencyResponseCalculations() -> int:
        return 2


    def _coeffsBA(self):
        """ return 12db/oct low pass biquad coeffs for given frequency """
        w = 2 * pi * self._frequency
        A = 1 / (tan((w / GRAPH_SR[0]) / 2))
        b0 = 1 / (1 + A)
        b1 = b0
        a1 = (1 - A) / (1 + A)
        return (b0, b1), (1, a1)
