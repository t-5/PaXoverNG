from typing import List

from PyQt5.QtGui import QColor
from persistent.list import PersistentList
from scipy import signal

from CustomWidgets.MixerWidget import MixerWidget
from DataClasses.XoverMixingObject import XoverMixingObject
from helpers.constants import GRAPH_FREQUS
from helpers.functions import mixColors


class XoverMixer(XoverMixingObject):
    """
    Crossover Mixer Class
    """

    def __init__(self, sink_name=""):
        super().__init__(sink_name)
        self._numOutputs = 2 # type: int
        self._outputs = PersistentList([None, None]) # type: PersistentList


    def camillaDspSnippet(self):
        ret = "  %s\n    channels:\n      in: %d\n      out: %d\n    mapping:\n" % \
              (self.sinkName(), len(self._v_fakeInputs), len(self._v_fakeOutputs))
        for outputIdx in range(len(self._v_fakeOutputs)):
            ret += "      - dest: %d\n        sources:\n" % outputIdx
            for inputIdx in range(len(self._v_fakeInputs)):
                if self._v_fakeInputs[inputIdx].isFake():
                    ret += "          - channel: %d\n            gain: 0\n            inverted: false\n" % outputIdx
                else:
                    try:
                        ret += "          - channel: %d\n            gain: %f\n            mute: %s\n            inverted: %s\n" % \
                           (inputIdx,
                            self._gainMatrix[inputIdx][outputIdx],
                            self._muteMatrix[inputIdx][outputIdx] and "true" or "false,",
                            self._invertMatrix[inputIdx][outputIdx] and "true" or "false")
                    except IndexError:
                        pass
        return ret


    def filterName(self) -> str:
        return "Mixer"


    def frequencyResponse(self, frequencies=GRAPH_FREQUS):
        """ return w, h frequency response components for mixer (straight line :) """
        b, a = (1, 1)
        return signal.freqz(b, a, worN=frequencies, whole=True)


    def info(self) -> str:
        return "" # not needed


    @staticmethod
    def isMixer():
        return True


    def restoreWidget(self, drawArea):
        """ restore widget """
        self._v_widget = MixerWidget(drawArea, self, self._widgetX, self._widgetY)
        self._v_widget.show()


    def setNumOutputs(self, value):
        self._numOutputs = value
        self._adjustOutputs()
        self._initGainMatrix()
        self._initInvertMatrix()
        self._initMuteMatrix()


    def _calculateOutputColors(self) -> List[QColor]:
        ret = []
        transposedGainMatrix = {}
        for output_idx in range(0, len(self._outputs)):
            transposedGainMatrix[output_idx] = []
        for input_idx, gains in enumerate(self._gainMatrix):
            for output_idx, gain in enumerate(gains):
                if self._muteMatrix[input_idx][output_idx]:
                    gain = -999
                transposedGainMatrix[output_idx].append(gain)
        for idx in range(0, len(self._outputs)):
            colors = []
            for input_ in self._inputs:
                if input_ is None:
                    colors.append(QColor(0, 0, 0))
                else:
                    colors.append(input_.fromObject().outputColors()[input_.fromIndex()])
            ret.append(mixColors(colors, transposedGainMatrix[idx]))
        return ret
