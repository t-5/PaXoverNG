from scipy import signal

from DataClasses.XoverFilter import XoverFilter
from helpers.constants import GRAPH_FREQUS
from helpers.functions import GRAPH_SR


class XoverDelay(XoverFilter):
    """
    Delay Base Class
    """


    def __init__(self, sink_name="", delay=0):
        super().__init__(sink_name)
        self._delay = delay # type: float


    def camillaDspSnippet(self):
        return "  %s:\n    type: Delay\n    delay: %f\n    unit: ms\n    subsample: false\n" % \
               (self.sinkName(), self._delay)


    def delay(self) -> float:
        return self._delay


    def delaySamples(self) -> float:
        return self._delay / 1000 * GRAPH_SR[0]


    @staticmethod
    def filterName() -> str:
        raise NotImplementedError


    def frequencyResponse(self, frequencies=GRAPH_FREQUS):
        """ return w, h frequency response components for filter """
        b, a = self._coeffsBA()
        return signal.freqz(b, a, worN=frequencies, whole=True)


    @staticmethod
    def gainGraphLowerLimit() -> int:
        return -15


    @staticmethod
    def gainGraphUpperLimit() -> int:
        return 1


    def info(self) -> str:
        return "Delay: %0.5fms (%0.1fmm)" % (self._delay, 343.2 * self._delay)


    def setDelay(self, delay):
        self._delay = delay
        self._v_dirty = True


    def parameterMappings(self) -> dict:
        if getattr(self, "_v_cached_parameterMappings", None) is None:
            ret = super().parameterMappings()[:]
            ret.append({
                "description": "Delay [ms]",
                "getter": self.delay,
                "setter": self.setDelay,
                "min": 0,
                "default": 0,
                "max": 10,
                "scale": "lin",
            })
            self._v_cached_parameterMappings = ret
        return self._v_cached_parameterMappings


    def _coeffsBA(self):
        raise NotImplementedError
