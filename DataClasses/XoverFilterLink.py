
try:
    from persistent import Persistent
    from persistent.list import PersistentList
except ImportError:
    Persistent = object
    PersistentList = object
    pass # import errors will be handled in MainWindow


class XoverFilterLink(Persistent):
    """
    Crossover Filter Link Class
    """

    def __init__(self):
        super().__init__()
        self._linkedFilters = PersistentList()


    def addLinkedFilter(self, flt):
        if not flt in self._linkedFilters:
            self._linkedFilters.append(flt)


    def allLinkedFilters(self):
        return self._linkedFilters


    def allLinkedFiltersExcept(self, flt):
        ret = []
        for tmp in self._linkedFilters:
            if tmp is not flt:
                ret.append(tmp)
        return ret


    def broadCastFrequencyExceptTo(self, flt, frequency):
        """ broadcast frequency change to all filters except <flt> """
        for destination in self._linkedFilters:
            if destination is not flt:
                destination.setFrequencyNoLinkUpdate(frequency)


    def merge(self, link):
        for flt in link.allLinkedFilters():
            self.addLinkedFilter(flt)


    def removeLinkedFilter(self, flt):
        if flt in self._linkedFilters:
            self._linkedFilters.remove(flt)
        # If there's only one filter remaining, remove the link, too.
        # It makes no sense to have a filter link with only one filter.
        if len(self._linkedFilters) == 1:
            self._linkedFilters.remove(self._linkedFilters[0])
