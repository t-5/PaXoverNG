from math import cos, pi, sin, sqrt

from scipy import signal

from DataClasses.XoverFilter import XoverFilter
from helpers.constants import GRAPH_FREQUS
from helpers.functions import GRAPH_SR


class XoverParamEq(XoverFilter):
    def __init__(self, sink_name:str="",
                 lowShelfFrequency:float=100, lowShelfGain:float=0, lowShelfQ:float=1,
                 eq1Frequency:float=300, eq1Gain:float=0, eq1Q:float=1,
                 eq2Frequency:float=1000, eq2Gain:float=0, eq2Q:float=1,
                 eq3Frequency:float=3000, eq3Gain:float=0, eq3Q:float=1,
                 highShelfFrequency:float=10000, highShelfGain:float=0, highShelfQ:float=1):
        super().__init__(sink_name)
        self._lowShelfFrequency = lowShelfFrequency # type: float
        self._lowShelfGain = lowShelfGain # type: float
        self._lowShelfQ = lowShelfQ # type: float
        self._eq1Frequency = eq1Frequency # type: float
        self._eq1Gain = eq1Gain # type: float
        self._eq1Q = eq1Q # type: float
        self._eq2Frequency = eq2Frequency # type: float
        self._eq2Gain = eq2Gain # type: float
        self._eq2Q = eq2Q # type: float
        self._eq3Frequency = eq3Frequency # type: float
        self._eq3Gain = eq3Gain # type: float
        self._eq3Q = eq3Q # type: float
        self._highShelfFrequency = highShelfFrequency # type: float
        self._highShelfGain = highShelfGain # type: float
        self._highShelfQ = highShelfQ # type: float


    def activeBands(self) -> int:
        return len(list(filter(lambda x: x != 0, (self._lowShelfGain,
                                                  self._eq1Gain,
                                                  self._eq2Gain,
                                                  self._eq3Gain,
                                                  self._highShelfGain))))


    def camillaDspSnippet(self): # TODO
        return "  %s:\n    type: FivePointPeq\n" \
               "    fls: %f\n    gls: %f\n    qls: %f\n" \
               "    fp1: %f\n    gp1: %f\n    qp1: %f\n" \
               "    fp2: %f\n    gp2: %f\n    qp2: %f\n" \
               "    fp3: %f\n    gp3: %f\n    qp3: %f\n" \
               "    fhs: %f\n    ghs: %f\n    qhs: %f\n" % \
               (self.sinkName(),
                self._lowShelfFrequency, self._lowShelfGain, self._lowShelfQ,
                self._eq1Frequency, self._eq1Gain, self._eq1Q,
                self._eq2Frequency, self._eq2Gain, self._eq2Q,
                self._eq3Frequency, self._eq3Gain, self._eq3Q,
                self._highShelfFrequency, self._highShelfGain, self._highShelfQ)


    def eq1Frequency(self) -> float:
        return self._eq1Frequency


    def eq1Gain(self) -> float:
        return self._eq1Gain


    def eq1Q(self) -> float:
        return self._eq1Q


    def eq2Frequency(self) -> float:
        return self._eq2Frequency


    def eq2Gain(self) -> float:
        return self._eq2Gain


    def eq2Q(self) -> float:
        return self._eq2Q


    def eq3Frequency(self) -> float:
        return self._eq3Frequency


    def eq3Gain(self) -> float:
        return self._eq3Gain


    def eq3Q(self) -> float:
        return self._eq3Q


    @staticmethod
    def filterName() -> str:
        return "Parametric EQ"


    def frequencyResponse(self, frequencies=GRAPH_FREQUS):
        """ return w, h frequency response components for all filters """
        hs = []
        b, a = self._coeffsBALowShelf()
        w, h = signal.freqz(b, a, worN=frequencies, whole=True)
        hs.append(h)
        b, a = self._coeffsBAPeaking1()
        w, h = signal.freqz(b, a, worN=frequencies, whole=True)
        hs.append(h)
        b, a = self._coeffsBAPeaking2()
        w, h = signal.freqz(b, a, worN=frequencies, whole=True)
        hs.append(h)
        b, a = self._coeffsBAPeaking3()
        w, h = signal.freqz(b, a, worN=frequencies, whole=True)
        hs.append(h)
        b, a = self._coeffsBAHighShelf()
        w, h = signal.freqz(b, a, worN=frequencies, whole=True)
        hs.append(h)
        # multiply all values in the five calculated frequency response hs
        for i in range(1,5):
            for j in range(0, len(hs[0])):
                hs[0][j] = hs[i][j] * hs[0][j]
        return w, hs[0]


    @staticmethod
    def gainGraphLowerLimit() -> int:
        return -20


    @staticmethod
    def gainGraphUpperLimit() -> int:
        return 20


    def highShelfFrequency(self) -> float:
        return self._highShelfFrequency


    def highShelfGain(self) -> float:
        return self._highShelfGain


    def highShelfQ(self) -> float:
        return self._highShelfQ


    def info(self) -> str:
        return "Active Bands: %d" % self.activeBands()


    def lowShelfFrequency(self) -> float:
        return self._lowShelfFrequency


    def lowShelfGain(self) -> float:
        return self._lowShelfGain


    def lowShelfQ(self) -> float:
        return self._lowShelfQ


    def parameterMappings(self) -> list:
        if getattr(self, "_v_cached_parameterMappings", None) is None:
            ret = super().parameterMappings()[:]
            ret.append({
                "description": "Low Shelf Frequency [Hz]",
                "getter": self.lowShelfFrequency,
                "setter": self.setLowShelfFrequency,
                "min": 2,
                "default": 100,
                "max": 20000,
                "scale": "log2",
            })
            ret.append({
                "description": "Low Shelf Gain [dB]",
                "getter": self.lowShelfGain,
                "setter": self.setLowShelfGain,
                "min": -20,
                "default": 0,
                "max": 20,
                "scale": "lin",
            })
            ret.append({
                "description": "Low Shelf Q",
                "getter": self.lowShelfQ,
                "setter": self.setLowShelfQ,
                "min": 0.1,
                "default": 0.707,
                "max": 10,
                "scale": "log10",
                "dividerAfter": True,
            })
            ret.append({
                "description": "EQ1 Center Frequency [Hz]",
                "getter": self.eq1Frequency,
                "setter": self.setEq1Frequency,
                "min": 2,
                "default": 300,
                "max": 20000,
                "scale": "log2",
            })
            ret.append({
                "description": "EQ1 Gain [dB]",
                "getter": self.eq1Gain,
                "setter": self.setEq1Gain,
                "min": -20,
                "default": 0,
                "max": 20,
                "scale": "lin",
            })
            ret.append({
                "description": "EQ1 Q",
                "getter": self.eq1Q,
                "setter": self.setEq1Q,
                "min": 0.1,
                "default": 0.707,
                "max": 10,
                "scale": "log10",
                "dividerAfter": True,
            })
            ret.append({
                "description": "EQ2 Center Frequency [Hz]",
                "getter": self.eq2Frequency,
                "setter": self.setEq2Frequency,
                "min": 2,
                "default": 1000,
                "max": 20000,
                "scale": "log2",
            })
            ret.append({
                "description": "EQ2 Gain [dB]",
                "getter": self.eq2Gain,
                "setter": self.setEq2Gain,
                "min": -20,
                "default": 0,
                "max": 20,
                "scale": "lin",
            })
            ret.append({
                "description": "EQ2 Q",
                "getter": self.eq2Q,
                "setter": self.setEq2Q,
                "min": 0.1,
                "default": 0.707,
                "max": 10,
                "scale": "log10",
                "dividerAfter": True,
            })
            ret.append({
                "description": "EQ3 Center Frequency [Hz]",
                "getter": self.eq3Frequency,
                "setter": self.setEq3Frequency,
                "min": 2,
                "default": 3000,
                "max": 20000,
                "scale": "log2",
            })
            ret.append({
                "description": "EQ3 Gain [dB]",
                "getter": self.eq3Gain,
                "setter": self.setEq3Gain,
                "min": -20,
                "default": 0,
                "max": 20,
                "scale": "lin",
            })
            ret.append({
                "description": "EQ3 Q",
                "getter": self.eq3Q,
                "setter": self.setEq3Q,
                "min": 0.1,
                "default": 0.707,
                "max": 10,
                "scale": "log10",
                "dividerAfter": True,
            })
            ret.append({
                "description": "High Shelf Frequency [Hz]",
                "getter": self.highShelfFrequency,
                "setter": self.setHighShelfFrequency,
                "min": 2,
                "default": 10000,
                "max": 20000,
                "scale": "log2",
            })
            ret.append({
                "description": "High Shelf Gain [dB]",
                "getter": self.highShelfGain,
                "setter": self.setHighShelfGain,
                "min": -20,
                "default": 0,
                "max": 20,
                "scale": "lin",
            })
            ret.append({
                "description": "High Shelf Q",
                "getter": self.highShelfQ,
                "setter": self.setHighShelfQ,
                "min": 0.1,
                "default": 0.707,
                "max": 10,
                "scale": "log10",
            })
            self._v_cached_parameterMappings = ret
        return self._v_cached_parameterMappings


    def setLowShelfFrequency(self, lowShelfFrequency):
        self._lowShelfFrequency = lowShelfFrequency
        self._v_dirty = True

    def setLowShelfGain(self, lowShelfGain):
        self._lowShelfGain = lowShelfGain
        self._v_dirty = True

    def setLowShelfQ(self, lowShelfQ):
        self._lowShelfQ = lowShelfQ
        self._v_dirty = True

    def setEq1Frequency(self, eq1Frequency):
        self._eq1Frequency = eq1Frequency
        self._v_dirty = True

    def setEq1Gain(self, eq1Gain):
        self._eq1Gain = eq1Gain
        self._v_dirty = True

    def setEq1Q(self, eq1Q):
        self._eq1Q = eq1Q
        self._v_dirty = True

    def setEq2Frequency(self, eq2Frequency):
        self._eq2Frequency = eq2Frequency
        self._v_dirty = True

    def setEq2Gain(self, eq2Gain):
        self._eq2Gain = eq2Gain
        self._v_dirty = True

    def setEq2Q(self, eq2Q):
        self._eq2Q = eq2Q
        self._v_dirty = True

    def setEq3Frequency(self, eq3Frequency):
        self._eq3Frequency = eq3Frequency
        self._v_dirty = True

    def setEq3Gain(self, eq3Gain):
        self._eq3Gain = eq3Gain
        self._v_dirty = True

    def setEq3Q(self, eq3Q):
        self._eq3Q = eq3Q
        self._v_dirty = True

    def setHighShelfFrequency(self, highShelfFrequency):
        self._highShelfFrequency = highShelfFrequency
        self._v_dirty = True

    def setHighShelfGain(self, highShelfGain):
        self._highShelfGain = highShelfGain
        self._v_dirty = True

    def setHighShelfQ(self, highShelfQ):
        self._highShelfQ = highShelfQ
        self._v_dirty = True


    def _coeffsBAHighShelf(self) -> tuple:
        """ return filter coefficients of high shelf filter """
        w0 = 2.0 * pi * self._highShelfFrequency / GRAPH_SR[0]
        alpha = sin(w0) / (2.0 * self._highShelfQ)
        A = pow(10, self._highShelfGain / 40.0)
        b0 =      A*((A+1.0) + (A-1.0)*cos(w0) + 2.0*sqrt(A)*alpha)
        b1 = -2.0*A*((A-1.0) + (A+1.0)*cos(w0))
        b2 =      A*((A+1.0) + (A-1.0)*cos(w0) - 2.0*sqrt(A)*alpha)
        a0 =         (A+1.0) - (A-1.0)*cos(w0) + 2.0*sqrt(A)*alpha
        a1 =    2.0*((A-1.0) - (A+1.0)*cos(w0))
        a2 =         (A+1.0) - (A-1.0)*cos(w0) - 2.0*sqrt(A)*alpha
        return (b0, b1, b2), (a0, a1, a2)


    def _coeffsBALowShelf(self) -> tuple:
        """ return filter coefficients of low shelf filter """
        w0 = 2.0 * pi * self._lowShelfFrequency / GRAPH_SR[0]
        alpha = sin(w0) / (2.0 * self._lowShelfQ)
        A = pow(10, self._lowShelfGain / 40.0)
        b0 =     A*( (A+1.0) - (A-1.0)*cos(w0) + 2.0*sqrt(A)*alpha)
        b1 = 2.0*A*( (A-1.0) - (A+1.0)*cos(w0))
        b2 =     A*( (A+1.0) - (A-1.0)*cos(w0) - 2.0*sqrt(A)*alpha)
        a0 =         (A+1.0) + (A-1.0)*cos(w0) + 2.0*sqrt(A)*alpha
        a1 =  -2.0*( (A-1.0) + (A+1.0)*cos(w0))
        a2 =         (A+1.0) + (A-1.0)*cos(w0) - 2.0*sqrt(A)*alpha
        return (b0, b1, b2), (a0, a1, a2)


    @staticmethod
    def _coeffsBAPeaking(f, g, q) -> tuple:
        """ return filter coefficients of peaking eq filter """
        w0 = 2.0 * pi * f / GRAPH_SR[0]
        alpha = sin(w0) / (2.0 * q)
        A = pow(10, g / 40.0)
        b0 =  1.0 + alpha * A
        b1 = -2.0 * cos(w0)
        b2 =  1.0 - alpha * A
        a0 =  1.0 + alpha / A
        a1 = -2.0 * cos(w0)
        a2 =  1.0 - alpha / A
        return (b0, b1, b2), (a0, a1, a2)


    def _coeffsBAPeaking1(self) -> tuple:
        return self._coeffsBAPeaking(self._eq1Frequency, self.eq1Gain(), self._eq1Q)


    def _coeffsBAPeaking2(self) -> tuple:
        return self._coeffsBAPeaking(self._eq2Frequency, self.eq2Gain(), self._eq2Q)


    def _coeffsBAPeaking3(self) -> tuple:
        return self._coeffsBAPeaking(self._eq3Frequency, self.eq3Gain(), self._eq3Q)
