from DataClasses.XoverLowHighPass import XoverLowHighPass
from helpers.functions import prettyFrequency


class XoverLowHighPassWithQ(XoverLowHighPass):
    """
    Crossover Low/High Pass With Q FBase Class
    """

    def __init__(self, sink_name="", frequency=1000, q=1):
        super().__init__(sink_name, frequency)
        self._q = q # type: float
        
        
    def camillaDspSnippet(self):
        raise NotImplementedError


    @staticmethod
    def filterName() -> str:
        raise NotImplementedError


    @staticmethod
    def frequencyResponseCalculations():
        raise NotImplementedError


    @staticmethod
    def gainGraphUpperLimit() -> int:
        return 15


    def info(self) -> str:
        return "Cutoff: %s | Q: %0.2f" % (prettyFrequency(self._frequency), self._q)


    def parameterMappings(self) -> list:
        if getattr(self, "_v_cached_parameterMappings", None) is None:
            ret = super().parameterMappings()[:]
            ret.append({
                "description": "Q",
                "getter": self.q,
                "setter": self.setQ,
                "min": 0.1,
                "default": 1,
                "max": 10,
                "scale": "log10",
            })
            self._v_cached_parameterMappings = ret
        return self._v_cached_parameterMappings


    def q(self) -> float:
        return self._q


    def setQ(self, q):
        self._q = q


    def _coeffsBA(self):
        raise NotImplementedError
