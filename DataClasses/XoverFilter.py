from __future__ import annotations

import time
from typing import Iterator

import numpy
from scipy import signal

from PyQt5.QtGui import QPixmap, QImage

from CustomWidgets.FilterWidget import FilterWidget
from CustomWidgets.XoverWidget import XoverWidget
from DataClasses.XoverConnectedObject import XoverConnectedObject
from DataClasses.XoverFilterLink import XoverFilterLink
from helpers.constants import GRAPH_FREQUS


class XoverFilter(XoverConnectedObject):
    """
    Crossover Filter Base Class
    """

    def __init__(self, sink_name=""):
        super().__init__()
        self._sink_name = sink_name # type: str
        self._frdFilename = "" # type: str
        self._frequencyResponsePng = b"" # type: bytes
        self._v_frequencyResponsePixmap = None
        self._v_dirty = True # type: bool
        self._v_lastModified = time.time() # type: float
        self._v_cached_parameterMappings = None
        self._v_widget = None


    def __repr__(self) -> str:
        return "<%s sink_name='%s' />" % (self.__class__.__name__, self._sink_name)


    def aggregatedFrequencyResponse(self, frequencies=GRAPH_FREQUS):
        if self._inputs[0] is None:
            return frequencies, numpy.ones(len(frequencies))
        wa, ha = self._inputs[0][1].aggregatedFrequencyResponse(frequencies=frequencies)
        w, h = self.frequencyResponse(frequencies=frequencies)
        return w, h * ha


    def applyFilterParams(self):
        """ apply filter params via websocekt """
        # TODO


    def camillaDspSnippet(self):
        raise NotImplementedError


    @staticmethod
    def canBeLinked() -> bool:
        return False


    def disconnectLinks(self):
        if not self.canBeLinked():
            return
        link = self.linkedBy()
        if not link:
            return
        link.removeLinkedFilter(self)


    @staticmethod
    def filterName() -> str:
        raise NotImplementedError


    def frequencyResponse(self, frequencies=GRAPH_FREQUS):
        raise NotImplementedError


    def frdFilename(self) -> str:
        return self._frdFilename


    def frequencyResponsePixmap(self) -> QPixmap:
        if getattr(self, "_v_frequencyResponsePixmap", None) is None:
            img = QImage.fromData(self._frequencyResponsePng)
            self._v_frequencyResponsePixmap = QPixmap(img)
        return self._v_frequencyResponsePixmap


    def gatherFrequencyResponsesUpToInput(self, whs):
        if len(self._inputs) == 0 or self._inputs[0] is None:
            b, a = ((1, 0, 0), (1, 0, 0))
            whs.append(signal.freqz(b, a, worN=GRAPH_FREQUS, whole=True))
        else:
            whs.append(self.frequencyResponse())
            self._inputs[0][1].gatherFrequencyResponsesUpToInput(whs)


    def info(self) -> str:
        raise NotImplementedError


    def isDirty(self) -> bool:
        if getattr(self, "_v_dirty", None) is None:
            self._v_dirty = False
        return self._v_dirty

    def lastModified(self) -> float:
        try:
            return self._v_lastModified
        except AttributeError:
            self._v_lastModified = time.time()
        return self._v_lastModified


    def linkedBy(self) -> XoverFilterLink | None:
        return None


    def markClean(self):
        self._v_dirty = False


    def markDirty(self):
        self._v_dirty = True
        self._v_lastModified = time.time()
        for output in self._outputs:
            if output is not None:
                output.toObject().markDirty()


    def outputWidgets(self) -> Iterator:
        return map(lambda x: x.toObject().widget(), self._outputs)


    def parameterMappings(self) -> list:
        return []


    @staticmethod
    def phaseLowerLimit() -> int:
        return -200


    @staticmethod
    def phaseUpperLimit() -> int:
        return 200


    def repaintWidget(self):
        self._v_widget.updateFrequencyResponseLabel()
        self._v_widget.repaint()


    def restoreWidget(self, drawArea):
        """ restore widget """
        self._v_widget = FilterWidget(drawArea, self, self._widgetX, self._widgetY)
        self._v_widget.show()


    def setFrdFilename(self, fn):
        self._frdFilename = fn
        self._v_dirty = True


    def setFrequencyResponsePng(self, png):
        self._frequencyResponsePng = png
        self._v_frequencyResponsePixmap = None


    def setSinkName(self, sink_name):
        self._sink_name = sink_name


    def sinkName(self) -> str:
        return self._sink_name


    def updateFrequencyResponsePng(self):
        w, h = self.frequencyResponse()
        png = self._v_widget.mainWindow().frequencyResponseGraphPngFor(w, h)
        self._frequencyResponsePng = png
        self._v_frequencyResponsePixmap = None
        self._v_widget.repaint()


    def updateOutputFrequencyResponsePngs(self):
        for output in self._outputs:
            if output is not None:
                output.toObject().updateOutputFrequencyResponsePngs()


    def widget(self) -> XoverWidget:
        return self._v_widget
