from typing import Iterator, List, Union

import numpy
from PyQt5.QtGui import QColor

try:
    from persistent.list import PersistentList
except ImportError:
    PersistentList = None # import errors will be handled in MainWindow

from CustomWidgets.InputWidget import InputWidget
from DataClasses.XoverConnectedObject import XoverConnectedObject
from helpers.constants import CHANNEL_COLORS, GRAPH_FREQUS


class XoverInput(XoverConnectedObject):
    """
    Crossover Input Class
    """

    def __init__(self):
        super().__init__()
        self._numChannels = 2 # type: int
        self._inputType = 'alsa' # type: str
        self._outputs = PersistentList([None, None]) # type: PersistentList
        self._v_widget = None


    @staticmethod
    def aggregatedCoeffsBA():
        return (1,), (1,)


    @staticmethod
    def aggregatedFrequencyResponse(frequencies=GRAPH_FREQUS):
        return frequencies, numpy.ones(len(frequencies))


    def info(self) -> str:
        return ""


    def inputs(self) -> Union[PersistentList, List]:
        return []


    def inputType(self) -> str:
        return self._inputType


    def numChannels(self) -> int:
        return self._numChannels


    def outputWidgets(self) -> Iterator:
        return map(lambda x: x and x[1].widget() or None, self._outputs)


    def restoreWidget(self, drawArea):
        """ restore widget """
        self._v_widget = InputWidget(drawArea, self, self._widgetX, self._widgetY)
        self._v_widget.show()


    def setInputType(self, type_: str):
        self._inputType = type_


    def setNumChannels(self, num: int):
        self._numChannels = num
        newOutputs = []
        for i in range(0, min(num, len(self._outputs))):
            newOutputs.append(self._outputs[i])
        for i in range(num, len(self._outputs)):
            if self._outputs[i]:
                self.disconnect(self._outputs[i])
        while len(newOutputs) < num:
            newOutputs.append(None)
        self._outputs = PersistentList(newOutputs)
        self._v_widget.resizeToZoomedSize()


    @staticmethod
    def sinkName() -> str:
        return "Input"


    def updateOutputFrequencyResponsePngs(self):
        for output in self._outputs:
            if output is not None:
                output.toObject().updateOutputFrequencyResponsePngs()


    def upstreamMixers(self) -> list:
        return []


    def _calculateOutputColors(self) -> List[QColor]:
        ret = []
        for idx in range(0, len(self._outputs)):
            ret.append(CHANNEL_COLORS[idx])
        return ret
