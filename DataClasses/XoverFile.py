import os
import shutil
import uuid
from collections.abc import Iterable
from tempfile import mkstemp
from time import time
from typing import List, Optional

from DataClasses.XoverConnectedObject import XoverConnectedObject
from DataClasses.XoverLinkwitzTransform import XoverLinkwitzTransform
from DataClasses.XoverObject import XoverObject
from WindowClasses.DrawArea import DrawArea
from helpers.constants import CURRENT_DATAMODEL_VERSION, SAMPLE_FORMAT_CONVERSION

try:
    import transaction
    from ZODB import FileStorage, DB
    from ZODB.broken import find_global
    from ZODB.serialize import referencesf
    from persistent.list import PersistentList
except ImportError:
    # import errors will be handled in MainWindow
    find_global = transaction =  FileStorage = referencesf = PersistentList = None
    DB = object

from DataClasses.XoverFilter import XoverFilter
from DataClasses.XoverInput import XoverInput
from DataClasses.XoverLowHighPass import XoverLowHighPass
from DataClasses.XoverOutput import XoverOutput


ALLOWED_MODULE_GLOBALS = (
    ("DataClasses.XoverConnection", "XoverConnection"),
    ("DataClasses.XoverFilterLink", "XoverFilterLink"),
    ("DataClasses.XoverFoHighpass", "XoverFoHighpass"),
    ("DataClasses.XoverFoLowpass", "XoverFoLowpass"),
    ("DataClasses.XoverHighPass", "XoverHighPass"),
    ("DataClasses.XoverInput", "XoverInput"),
    ("DataClasses.XoverLinkwitzTransform", "XoverLinkwitzTransform"),
    ("DataClasses.XoverLr2Highpass", "XoverLr2Highpass"),
    ("DataClasses.XoverLr2Lowpass", "XoverLr2Lowpass"),
    ("DataClasses.XoverLr4Highpass", "XoverLr4Highpass"),
    ("DataClasses.XoverLr4Lowpass", "XoverLr4Lowpass"),
    ("DataClasses.XoverMixer", "XoverMixer"),
    ("DataClasses.XoverOutput", "XoverOutput"),
    ("DataClasses.XoverParamEq", "XoverParamEq"),
    ("DataClasses.XoverSampleDelay", "XoverSampleDelay"),
    ("DataClasses.XoverSoHighpass", "XoverSoHighpass"),
    ("DataClasses.XoverSoHighpassWithQ", "XoverSoHighpassWithQ"),
    ("DataClasses.XoverSoLowpass", "XoverSoLowpass"),
    ("DataClasses.XoverSoLowpassWithQ", "XoverSoLowpassWithQ"),
    ("DataClasses.XoverSubsampleDelay", "XoverSubsampleDelay"),
    ("DataClasses.XoverToHighpass", "XoverToHighpass"),
    ("DataClasses.XoverToLowpass", "XoverToLowpass"),
    ("persistent.list", "PersistentList"),
    ("persistent.mapping", "PersistentMapping"),
    ("uuid", "SafeUUID"),
    ("uuid", "UUID"),
)


class XoverDB(DB):
    """
    derived from ZODB.DB.DB - overrides classFactory() to only allow
    import of very specific classes
    """
    def classFactory(self, connection, modulename, globalname):
        if (modulename, globalname) not in ALLOWED_MODULE_GLOBALS:
            raise TypeError("Not allowed to import global %r from module %r" % (globalname, modulename))
        return super().classFactory(connection, modulename, globalname)


class XoverFile(object):
    """
    Crossover configuration file class, acts as a wrapper around
    the ZODB FilesStorage contained in it and handles copying to
    and from tempfiles in the background
    """
    def __init__(self, filename: str):
        """
        initializes a new XoverFile instance
        :param filename: give the filename of the FileStorage file,
                         if filename is empty, only init temp files.
        """
        self._filename = filename
        self._dirty = False
        fh, self._tempname = mkstemp(prefix="PaXoverNG_", suffix=".fs")
        os.close(fh)
        if filename != "":
            shutil.copyfile(self._filename, self._tempname)
        self._zodb_storage = FileStorage.FileStorage(self._tempname)
        self._zodb_db = XoverDB(self._zodb_storage)
        self._zodb_connection = self._zodb_db.open()
        self._zodb_root = self._zodb_connection.root()
        self._bootstrap()


    def addObject(self, obj: XoverObject):
        self._zodb_root["allObjects"].append(obj)
        self.markDirty()


    def allConnectedObjects(self):
        ret = list(filter(lambda x: issubclass(x.__class__, XoverConnectedObject),
                          self._zodb_root["allObjects"]))
        return ret


    def allFilters(self, sort:bool=False):
        ret = list(filter(lambda x: issubclass(x.__class__, XoverFilter),
                          self._zodb_root["allObjects"]))
        if sort:
            ret.sort(key=XoverFilter.sinkName)
        return ret


    def allLinkableFilters(self, sort:bool=False) -> List[XoverObject]:
        ret = list(filter(lambda x: issubclass(x.__class__, XoverFilter) and x.canBeLinked(),
                          self._zodb_root["allObjects"]))
        if sort:
            ret.sort(key=XoverFilter.sinkName)
        return ret


    def applyFilterParams(self):
        """ apply filter params via camilladsp websocket """
        for obj in self._zodb_root["allObjects"]:
            obj.applyFilterParams()


    def close(self):
        """ close db connection and remove temporary db files """
        transaction.abort()
        self._zodb_connection.close()
        self._zodb_root = None
        # cleanup db files in TMP dir
        for fn in (self._tempname,
                   "%s.index" % self._tempname,
                   "%s.index_tmp" % self._tempname,
                   "%s.lock" % self._tempname,
                   "%s.old" % self._tempname,
                   "%s.pack" % self._tempname,
                   "%s.tmp" % self._tempname):
            try:
                os.unlink(fn)
            except OSError:
                pass


    def camillaDspConfig(self) -> str:
        """
        return
        - camilladsp config file contents as string
        """

        # TODO: no open connections allowed

        input_ = self.xoverInput()
        output = self.xoverOutput()

        # header
        ret = "devices:\n"
        ret += "  samplerate: %d\n" % output.sampleRate()
        ret += "  chunksize: %d\n" % output.chunkSize()
        ret += "  silence_threshold: -80\n"

        # capture device
        ret += "  capture:\n"
        ret += "    type: File\n"
        ret += "    channels: %d\n" % input_.numOutputs()
        ret += "    format: $format$\n"
        ret += "    filename: /dev/sdtin\n"

        # playback device
        output = self.xoverOutput()
        ret += "  playback:\n"
        ret += "    type: Alsa\n"
        ret += "    channels: %d\n" % len(output.inputs())
        ret += "    format: %s\n" % SAMPLE_FORMAT_CONVERSION[output.sampleFormat()]
        ret += "    device: %s\n" % output.device()

        # reset fake channels
        for o in self.allConnectedObjects():
            o.resetFakeChannels()

        # slice the object tree into sections between mixers and store fake channels
        allMixers = list(input_.allMixers().values())
        allMixers.reverse()

        # first reset all fake channels
        for mixer in allMixers:
            mixer.resetFakeChannels()
        output.resetFakeChannels()




        # mixer snippets
        ret += "mixers:\n"
        for mixer in reversed(allMixers):
            ret += mixer.camillaDspSnippet()

        # assemble pipeline

        return ret


    def dbRoot(self):
        """ return zodb root """
        return self._zodb_root


    def delObject(self, obj):
        if obj in self._zodb_root["allObjects"]:
            self._zodb_root["allObjects"].remove(obj)
        self.markDirty()


    def fileName(self):
        """ return filename """
        return self._filename


    def filterSinkNames(self) -> Iterable:
        return map(lambda x: x.sinkName(),
                   filter(lambda x: issubclass(x.__class__,  XoverFilter),
                          self._zodb_root["allObjects"]))


    def isDirty(self) -> bool:
        """ return dirty flag """
        return self._dirty


    def isUniqueName(self, name: str, exclude=None) -> bool:
        for obj in self._zodb_root["allObjects"]:
            if obj == exclude:
                continue
            if obj.sinkName() == name:
                return False
        return True


    def lowHighPassFilters(self) -> List[XoverObject]:
        # also include Linkwitz Transform filters here!
        return list(filter(lambda x: issubclass(x.__class__, XoverLowHighPass) or \
                                     issubclass(x.__class__, XoverLinkwitzTransform),
                           self._zodb_root["allObjects"]))


    def markClean(self):
        self._dirty = False


    def markDirty(self):
        self._dirty = True


    def nameExists(self, name) -> bool:
        for obj in self._zodb_root["allObjects"]:
            if obj.sinkName() == name:
                return True
        return False


    def outputSinkNames(self) -> Iterable:
        return map(lambda x: x.outputSinkName(),
                   filter(lambda x: x.__class__ is XoverOutput,
                          self._zodb_root["allObjects"]))


    def restoreWidgets(self, drawArea: DrawArea):
        drawArea.clear()
        for obj in self._zodb_root["allObjects"]:
            obj.restoreWidget(drawArea)


    def save(self):
        """ save temporary db to original filename """
        self.saveAs(self._filename, needNewUuid=False)


    def saveAs(self, filename:str, needNewUuid:bool=True):
        """ save temporary db to given filename """
        if needNewUuid:
            self._zodb_root["UUID"] = uuid.uuid4()
        self._zodb_root["saved_version"] = CURRENT_DATAMODEL_VERSION
        transaction.commit()
        self._zodb_storage.pack(time(), referencesf)
        shutil.copyfile(self._tempname, filename)
        self._filename = filename
        self.markClean()


    def savedVersion(self) -> str:
        return self._zodb_root["saved_version"]


    def uuid(self) -> str:
        return self._zodb_root["UUID"]


    def xoverFilters(self) -> List[XoverFilter]:
        return list(filter(lambda x: issubclass(x.__class__, XoverFilter),
                           self._zodb_root["allObjects"]))


    def xoverInput(self) -> XoverInput:
        return self._zodb_root["xoverInput"]


    def xoverOutput(self) -> Optional[XoverOutput]:
        ret = list(filter(lambda x: issubclass(x.__class__, XoverOutput),
                          self._zodb_root["allObjects"]))
        if ret:
            return ret[0]
        return None


    def _bootstrap(self):
        """ setup internal structure of ZODB """
        root = self._zodb_root
        if not "saved_version" in root:
            root["saved_version"] = 0
            self.markDirty()
        if root["saved_version"] < 1:
            # Initial setup of containers and the UUID
            if not "UUID" in root:
                root["UUID"] = str(uuid.uuid4())
                self.markDirty()
                transaction.commit()
            if not "allObjects" in root:
                root["allObjects"] = PersistentList()
                self.markDirty()
                transaction.commit()
            if not "xoverInput" in root:
                xoverInput = XoverInput()
                root["xoverInput"] = xoverInput
                self.addObject(xoverInput)
                transaction.commit()
    # if root["saved_version"] < 2: ... TBD
