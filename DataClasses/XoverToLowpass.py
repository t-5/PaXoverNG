from math import pi

import numpy

from DataClasses.XoverLowPass import XoverLowPass
from helpers.functions import GRAPH_SR


class XoverToLowpass(XoverLowPass):
    """
    Crossover Third Order Butterworth Low-Pass Filter Class
    """
    def __init(self, sink_name:str="", fequency:float=1000):
        super().__init__(sink_name, fequency)


    def camillaDspSnippet(self):
        return "  %s:\n    type: ButterworthHighpass\n    freq: %f\n    order: 3\n" % \
               (self.sinkName(), self._frequency)


    @staticmethod
    def filterName() -> str:
        return "Third Order Butterworth Lowpass"


    @staticmethod
    def frequencyResponseCalculations() -> int:
        return 1


    def _coeffsBA(self):
        """ return 18db/oct low pass biquad coeffs for given frequency """
        # this code is intentionally broken out into pieces to make it
        # simpler to keep it in sync with the C implementation used in
        # ladspa-t5-plugins
        Wn = 2 * self._frequency / GRAPH_SR[0]
        p = []
        for m in (-2, 0, 2):
            p.append(-numpy.exp(1j * pi * m / 6))
        p = numpy.array(p)
        warped = 4 * numpy.tan(pi * Wn / 2)
        z_analog = numpy.array([])
        z_digital = numpy.array(-numpy.ones(3))
        p_analog = warped * p
        k_analog = warped ** 3
        p_digital = (4 + p_analog) / (4 - p_analog)
        k_digital = k_analog * numpy.real(numpy.prod(4 - z_analog) / numpy.prod(4 - p_analog))
        b = k_digital * numpy.poly(z_digital)
        a = numpy.poly(p_digital)
        return b, a
