from DataClasses.XoverDelay import XoverDelay


class XoverSampleDelay(XoverDelay):
    """
    Sample Accurate Delay Filter Class
    """
    def __init(self, sink_name:str="", delay:float=0):
        super().__init__(sink_name, delay=delay)


    @staticmethod
    def filterName() -> str:
        return "Sample Accurate Delay"


    def _coeffsBA(self):
        """ return linear frequency response """
        ## removed for now
        # prepend <delaySamples> zeros to coefficients
        #ret = numpy.zeros(int(self.delaySamples()) + 1)
        #ret[-1] = 1
        return (1,), (1,)
