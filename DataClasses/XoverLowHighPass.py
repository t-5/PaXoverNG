from scipy import signal

from DataClasses.XoverFilter import XoverFilter
from helpers.constants import GRAPH_FREQUS
from helpers.functions import prettyFrequency


class XoverLowHighPass(XoverFilter):
    """
    Crossover Low/High Pass Filter Base Class
    """

    @staticmethod
    def filterName():
        pass

    def __init__(self, sink_name="", frequency=1000):
        super().__init__(sink_name)
        self._frequency = frequency # type: float
        self._linkedBy = None


    def camillaDspSnippet(self):
        raise NotImplementedError


    @staticmethod
    def canBeLinked() -> bool:
        return True


    def disconnectAll(self):
        super().disconnectAll()
        if self._linkedBy is not None:
            self._linkedBy.removeLinkedFilter(self)


    def frequency(self) -> float:
        return self._frequency


    def frequencyResponse(self, frequencies=GRAPH_FREQUS):
        """ return w, h frequency response components for filter """
        hs = []
        b, a = self._coeffsBA()
        w, h = signal.freqz(b, a, worN=frequencies, whole=True)
        hs.append(h)
        hs.append(h)
        # maybe do filter calculation twice, as a lr-4 filter is two
        # 12dB/oct filters stacked on top of each other
        for i in range(1, self.frequencyResponseCalculations()):
            for j in range(0, len(hs[0])):
                hs[0][j] = hs[i][j] * hs[0][j]
        return w, hs[0]


    @staticmethod
    def frequencyResponseCalculations():
        raise NotImplementedError


    @staticmethod
    def gainGraphLowerLimit() -> int:
        return -30


    @staticmethod
    def gainGraphUpperLimit() -> int:
        return 2


    def info(self) -> str:
        return "Cutoff: %s" % prettyFrequency(self._frequency)


    def linkedBy(self):
        return self._linkedBy


    def parameterMappings(self) -> list:
        if getattr(self, "_v_cached_parameterMappings", None) is None:
            ret = super().parameterMappings()[:]
            ret.append({
                "description": "Cutoff Frequency [Hz]",
                "getter": self.frequency,
                "setter": self.setFrequency,
                "min": 2,
                "default": 1000,
                "max": 20000,
                "scale": "log2",
            })
            self._v_cached_parameterMappings = ret
        return self._v_cached_parameterMappings


    def setFrequency(self, frequency):
        self._frequency = frequency
        self._v_dirty = True
        if self._linkedBy is not None:
            self._linkedBy.broadCastFrequencyExceptTo(self, frequency)


    def setFrequencyNoLinkUpdate(self, frequency):
        self._frequency = frequency
        self._v_dirty = True


    def setLinkedBy(self, linkedBy):
        self._linkedBy = linkedBy
        self._v_dirty = True


    def _coeffsBA(self):
        raise NotImplementedError
