import math
import os
import re
import subprocess
from functools import reduce
from typing import Any, Dict, List, Tuple, Union

import numpy
from PyQt5.QtGui import QColor

try:
    import pulsectl
except ImportError:
    pulsectl = None # the import error is handled in MainWindow.py


from helpers.constants import CHANNEL_COLORS, PA_CLIENT_NAME, CONSOLE_ENCODING

_PULSE = None
_PULSE_STATUS = ["DSP: N/A", "HW: N/A"]
_TESTING_SEED = 0
GRAPH_SR = [192000]


def alsacap():
    """
    calls the alsacap command line utility and return a parsed structure

$ pasuspender alsacap
*** Scanning for playback devices ***
Card 0, ID `Generic', name `HD-Audio Generic'
  Device 3, ID `HDMI 0', name `HDMI 0', 1 subdevices (1 available)
    2 channels, sampling rate 32000..48000 Hz
    Sample formats: S16_LE, S32_LE
      Subdevice 0, name `subdevice #0'
Card 1, ID `Generic_1', name `HD-Audio Generic'
  Device 0, ID `ALC257 Analog', name `ALC257 Analog', 1 subdevices (1 available)
    2 channels, sampling rate 44100..48000 Hz
    Sample formats: S16_LE, S32_LE
      Subdevice 0, name `subdevice #0'
Card 2, ID `U5', name `ASUS XONAR U5'
  Device 0, ID `USB Audio', name `USB Audio', 1 subdevices (1 available)
    2..6 channels, sampling rate 44100..192000 Hz
    Sample formats: S16_LE, S24_3LE
      Subdevice 0, name `subdevice #0'
  Device 1, ID `USB Audio', name `USB Audio #1', 1 subdevices (1 available)
    2 channels, sampling rate 44100..192000 Hz
    Sample formats: S16_LE, S24_3LE
      Subdevice 0, name `subdevice #0'
  Device 2, ID `USB Audio', name `USB Audio #2', 1 subdevices (1 available)
    2 channels, sampling rate 44100..192000 Hz
    Sample formats: S16_LE
      Subdevice 0, name `subdevice #0'
Card 3, ID `Audio', name `USB Audio'
    """
    # kill camilladsp processes to release soundcards in use
    # these should be restarted automatically, at least when under our control
    os.system("killall camilladsp")
    # first check if pulseaudio is running because then we have to use pasuspender
    ret = os.popen("ps x|egrep '/usr/bin/pulseaudio(\s.*?)$'").read()
    if "/usr/bin/pulseaudio" in ret:
        ret = os.popen("/usr/bin/pasuspender /usr/bin/alsacap").read()
    else:
        ret = os.popen("/usr/bin/alsacap").read()
    cards = []
    last_device_key = None
    for line in ret.split("\n"):
        if line.startswith("Card "):
            card, id_, name = line.split(", ")
            card = int(card.replace("Card ", ""))
            id_ = id_.replace("ID `", "").replace("'", "")
            name = name.replace("name `", "").replace("'", "")
            cards.append({"num": card, "id": id_, "name": name, "devices": {}})
        elif line.startswith("  Device "):
            device, id_, name, _ = line.split(", ")
            device = int(device.replace("  Device ", ""))
            id_ = id_.replace("ID `", "").replace("'", "")
            name = name.replace("name `", "").replace("'", "")
            cards[-1]["devices"][device] = {"num": device, "id": id_, "name": name, "subdevices": []}
            last_device_key = device
        elif re.match(' {4}([0-9.])+ channels', line):
            matches = re.match(' {4}([0-9.]+) channels, sampling rate ([0-9.]+)', line)

            channels = matches.group(1)
            if ".." in channels:
                min_channels, max_channels = map(lambda x: int(x), channels.split(".."))
            else:
                min_channels = max_channels = int(channels)
            cards[-1]["devices"][last_device_key]["min_channels"] = min_channels
            cards[-1]["devices"][last_device_key]["max_channels"] = max_channels

            samplerates = matches.group(2)
            if ".." in samplerates:
                min_samplerate, max_samplerate = map(lambda x: int(x), samplerates.split(".."))
            else:
                min_samplerate = max_samplerate = int(samplerates)
            cards[-1]["devices"][last_device_key]["min_samplerate"] = min_samplerate
            cards[-1]["devices"][last_device_key]["max_samplerate"] = max_samplerate
        elif line.startswith("    Sample formats: "):
            _, formats = line.split("Sample formats: ")
            formats = formats.split(", ")
            cards[-1]["devices"][last_device_key]["formats"] = formats
    return cards


def bezierIntermediatesFor(start, dest) -> tuple:
    startY = start.y()
    startX = start.x()
    destX = dest.x()
    destY = dest.y()
    # intermediary point 1
    im1X = startX + (destX - startX) / 2
    im1Y = startY
    # intermediary point 1
    im2X = im1X
    im2Y = destY
    return im1X, im1Y, im2X, im2Y


def cmdRunAndRead(cmd, returnStdErr=False) -> Union[Tuple[str, str], str]:
    with subprocess.Popen(cmd,
                          stdin=subprocess.PIPE,
                          stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE,
                          shell=True) as proc:
        cmd_stdout_bytes, cmd_stderr_bytes = proc.communicate(b'')
        cmd_stdout, cmd_stderr = cmd_stdout_bytes.decode(CONSOLE_ENCODING), cmd_stderr_bytes.decode(CONSOLE_ENCODING)
        if returnStdErr:
            return cmd_stdout, cmd_stderr
        return cmd_stdout


def colorForChannelNum(num) -> QColor:
    return CHANNEL_COLORS.get(num, QColor(150, 150, 150))


def colorForChannelNumRgbString(num) -> str:
    color = colorForChannelNum(num)
    return "rgb(%d,%d,%d)" % (color.red(), color.green(), color.blue())


def configSnippetToCommandList(configSnippet) -> list:
    return list(map(lambda x: "pacmd %s" % x,
                    filter(lambda x: x != "" and not x.startswith("#"),
                           configSnippet.split("\n"))))


def dbToGainFactor(db) -> float:
    return pow(10, db / 20.0)


def gainFactorToDb(gain) -> float:
    return 20 * math.log10(gain)


def interpolateWeightedLinear(targetFrequency, sourceFrequencies, sourceValues):
    """
    do a weighted linear interpolation and return a magnitude for a given target
    frequency, interpolating from a list of source frequencies and source magnitudes
    """
    for idx, sourceFrequency in enumerate(sourceFrequencies):
        if sourceFrequency > targetFrequency:
            try:
                prevValue = sourceValues[idx - 1]
                prevFrequency = sourceFrequencies[idx - 1]
            except ValueError:
                prevValue = sourceValues[idx]
                prevFrequency = sourceFrequencies[idx]
            currentMagnitude = sourceValues[idx]
            currentFrequency = sourceFrequencies[idx]
            weight = (targetFrequency - prevFrequency) / (currentFrequency - prevFrequency)
            return prevValue * (1 - weight) + currentMagnitude * weight
    return sourceValues[-1]


def keepPhaseBounded(phase):
    if type(phase) is numpy.ndarray:
        ret = numpy.copy(phase)
        for idx, value in enumerate(ret):
            while value > 180:
                value -= 360
            while value < -180:
                value += 360
            ret[idx] = value
        return ret
    # assume single value
    ret = phase
    while ret > 180:
        ret -= 360
    while ret < -180:
        ret += 360
    return ret


def mixColors(colors: List[QColor], gains: List[float]) -> QColor:
    """
    mix QColors determined by the ratio between gains expressed in dB
    """
    gainFactors = list(map(lambda gain: dbToGainFactor(gain), gains))
    gainSum = reduce(lambda a, c: a + c, gainFactors)
    ratios = []
    for gainFactor in gainFactors:
        if gainSum == 0:
            ratios.append(gainFactor / 0.000001)
        else:
            ratios.append(gainFactor / gainSum)
    idxs = range(0, len(gains))
    r = 0
    g = 0
    b = 0
    for idx in idxs:
        try:
            r += ratios[idx] * colors[idx].red()
        except IndexError:
            pass
        try:
            g += ratios[idx] * colors[idx].green()
        except IndexError:
            pass
        try:
            b += ratios[idx] * colors[idx].blue()
        except IndexError:
            pass
    m = max(r, g, b)
    if m > 0:
        r = r / m * 255
        g = g / m * 255
        b = b / m * 255
    return QColor(int(round(r)), int(round(g)), int(round(b)))


def parse_frd_file(filename: str) -> Tuple[List[Tuple[float, float, float]], str]:
    """
    parses a .frd frequency/phase response text file and returns a
    list of tuples (frequency, magnitude, phase) with parsed content
    and an error string which is empty in case of no errors
    """
    ret = []
    errors = ""
    r = re.compile(r"([0-9.+Ee-]+),?\s+([0-9.+Ee-]+),?\s+([0-9.+Ee-]+)")
    try:
        with open(filename, "rb") as f:
            contentBytes = f.read()
    except (FileNotFoundError, PermissionError) as e:
        return ret, "%s\n" % e
    content = ""
    for encoding in ("utf8", "ISO8859-15"):
        try:
            content = contentBytes.decode(encoding)
        except UnicodeDecodeError:
            pass
        if content != "":
            break
    for number, line in enumerate(content.splitlines()):
        if line.startswith("*"): # * marks a comment
            continue
        m = r.search(line)
        if m:
            try:
                ret.append((float(m.group(1)),
                            float(m.group(2)),
                            float(m.group(3))))
            except ValueError:
                errors += "Error parsing line %d (content: '%r')\n" % (number, line)
    return ret, errors


def prettyFrequency(f: float) -> str:
    """ prettify a frequency number for display """
    if f < 1000:
        return "%d Hz" % f
    return "%0.2f kHz" % (f / 1000)


def pulse_get_default_sink() -> str:
    """ return default sink name """
    global _PULSE
    try:
        if _PULSE is None:
            _PULSE = pulsectl.Pulse(PA_CLIENT_NAME)
        return _PULSE.server_info().default_sink_name
    except pulsectl.pulsectl.PulseError:
        _PULSE = None
        return ""


def pulse_get_hw_sinks() -> List[Dict[str, Union[str, Any]]]:
    """ return hardware sink names/descriptions """
    global _PULSE
    try:
        if _PULSE is None:
            _PULSE = pulsectl.Pulse(PA_CLIENT_NAME)
        ret = []
        seen_sink_descriptions = set()
        for _sinkinfo in _PULSE.sink_list():
            if _sinkinfo.proplist.get('device.master_device', None) is None:
                num = 1
                description = _sinkinfo.description
                while description in seen_sink_descriptions:
                    num += 1
                    description = "%s * %d" % (_sinkinfo.description, num)
                ret.append({'name': _sinkinfo.name, 'description': description})
                seen_sink_descriptions.add(description)
        return ret
    except pulsectl.pulsectl.PulseError:
        _PULSE = None
        return []


def pulse_get_sink_channels(sink_name: str) -> List[str]:
    foundName = None
    lines = cmdRunAndRead("pacmd list-sinks")
    sink_name_match = "name: <%s>" % sink_name
    for line in lines.split("\n"):
        if sink_name_match in line:
            foundName = sink_name
        if foundName == sink_name:
            if "channel map: " in line:
                return line.strip().split(": ")[-1].split(",")
    return []


def pulse_get_sink_volume(sink_name: str) -> float:
    """ return sink volume as float """
    global _PULSE
    try:
        if _PULSE is None:
            _PULSE = pulsectl.Pulse(PA_CLIENT_NAME)
        for _sinkinfo in _PULSE.sink_list():
            if _sinkinfo.name == sink_name:
                return _sinkinfo.volume.value_flat
    except pulsectl.pulsectl.PulseError:
        _PULSE = None
        return 0.0


def pulse_get_status(output_sink: str, resetHwStatus:bool=False) -> List[str]:
    """ return textual status of pulseaduio system """
    global _PULSE, _PULSE_STATUS, GRAPH_SR
    try:
        if _PULSE is None:
            _PULSE = pulsectl.Pulse(PA_CLIENT_NAME)
        if _PULSE_STATUS[0] == "DSP: N/A":
            ret = cmdRunAndRead("LC_ALL=c pacmd info|grep 'Default sample spec:'").rstrip().split()
            if len(ret) == 6:
                sr = int(ret[-1].replace("Hz", ""))
                GRAPH_SR[0] = sr
                _PULSE_STATUS[0] = "DSP: %0.1fkHz/%s" % (sr / 1000, ret[-3])
            else:
                _PULSE_STATUS[0] = "DSP: N/A"
        if resetHwStatus:
            _PULSE_STATUS[1] = "HW: N/A"
        if _PULSE_STATUS[1] == "HW: N/A":
            ret = cmdRunAndRead("LC_ALL=c pacmd list sinks|grep \"name: <%s>\" -A 20|grep 'sample spec:'" % output_sink).rstrip().split()
            if len(ret) == 5:
                sr = int(ret[-1].replace("Hz", ""))
                GRAPH_SR[0] = sr
                _PULSE_STATUS[1] = "HW: %0.1fkHz/%s" % (sr / 1000, ret[-3])
            else:
                _PULSE_STATUS[1] = "HW: N/A"
    except pulsectl.pulsectl.PulseError:
        _PULSE = None
        _PULSE_STATUS = ["DSP: N/A", "HW: N/A"]
        GRAPH_SR[0] = 192000
    return _PULSE_STATUS


def pulse_set_sink_volume(sink_name: str, volume: float):
    """ set volume from float """
    global _PULSE
    try:
        if _PULSE is None:
            _PULSE = pulsectl.Pulse(PA_CLIENT_NAME)
        for _sinkinfo in _PULSE.sink_list():
            if _sinkinfo.name == sink_name:
                _PULSE.volume_set_all_chans(_sinkinfo, volume)
    except pulsectl.pulsectl.PulseError:
        _PULSE = None


def setupPltFigure(w, h, mplCanvas, mplFigure,
                   gainLowerLimit, gainUpperLimit,
                   phaseLowerLimit, phaseUpperLimit,
                   phaseLineThickness=2.0):
    """ helper function to setup a mathplotlib canvas/figure """
    mplFigure.patch.set_facecolor("#333333")
    ax1 = mplFigure.add_subplot(111)
    ax1.set_zorder(10)
    ax1.patch.set_visible(False)
    ax1.grid(which='major', axis='both', color="#dddddd", linestyle='-')
    ax1.grid(which='minor', axis='x', color="#bbbbbb", linestyle='--')
    ax1.spines['bottom'].set_color('#dddddd')
    ax1.spines['top'].set_color('#dddddd')
    ax1.spines['right'].set_color('#dddddd')
    ax1.spines['left'].set_color('#dddddd')
    ax1.tick_params(axis='x', colors='#dddddd')
    ax1.tick_params(axis='y', colors='#dddddd')
    pltLine1, = ax1.plot(w, 20 * numpy.log10(abs(h)), '#00ff00', linewidth=2.0, zorder=100)
    # noinspection PyTypeChecker
    ax1.axis([10, GRAPH_SR[0] / 2, gainLowerLimit, gainUpperLimit])
    ax1.set_ylabel('Amplitude [dB]', color='#00ff00')
    ax1.set_xscale('log')
    ax2 = ax1.twinx()
    ax2.patch.set_facecolor("#111111")
    ax2.patch.set_visible(True)
    ax2.spines['bottom'].set_color('#dddddd')
    ax2.spines['top'].set_color('#dddddd')
    ax2.spines['right'].set_color('#dddddd')
    ax2.spines['left'].set_color('#dddddd')
    ax2.tick_params(axis='x', colors='#dddddd')
    ax2.tick_params(axis='y', colors='#dddddd')
    angles = keepPhaseBounded(numpy.unwrap(numpy.angle(h)) / math.pi * 180)
    pltLine2, = ax2.plot(w, angles, color='#ff6666', zorder=-10, linewidth=phaseLineThickness)
    ax2.set_ylabel('Phase [degrees]', color='#ff6666')
    ax2.grid(False)
    ax2.set_ylim(phaseLowerLimit, phaseUpperLimit)
    ax2.set_xlim(10, GRAPH_SR[0] / 2)
    mplFigure.tight_layout(pad=0)
    mplCanvas.draw()
    return pltLine1, ax1, pltLine2, ax2


def testingIdGeneratorReset():
    """ used for testing """
    global _TESTING_SEED
    _TESTING_SEED = 0


def testingIdGeneratorGet() -> int:
    """ used for testing """
    global _TESTING_SEED
    _TESTING_SEED += 1
    return _TESTING_SEED
