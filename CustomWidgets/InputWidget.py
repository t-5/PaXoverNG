from PyQt5.QtCore import Qt, QRect
from PyQt5.QtGui import QColor, QFont, QIcon, QPen, QPixmap
from PyQt5.QtWidgets import QToolButton

from CustomWidgets.XoverWidget import XoverWidget
from WindowClasses.EditInputDialog import EditInputDialog
from helpers.constants import INPUT_TYPE_MAPPING, PA_CHANNEL_NAMES
from helpers.functions import colorForChannelNum


class InputWidget(XoverWidget):

    LINECOLOR = QColor(180, 255, 180)
    CAPTION_BGCOLOR = QColor(0, 102, 0)
    HEIGHT = 85
    WIDTH = 130



    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._addCaptionButtons()


    def inputDotRects(self):
        return []


    def onEdit(self):
        dlg = EditInputDialog(self._mainWindow, self._xoverObject)
        dlg.exec()
        self.mainWindow().markDirty()
        self.resizeToZoomedSize()
        self.repaint()


    def paint(self, qp):
        self._drawOutputDots(qp)
        qp.setPen(self.LINECOLOR)
        qp.setBrush(self.BGCOLOR)
        qp.drawRect(0, 0,
                    self.width() - 1 - self._zoom(self.DOT_RADIUS),
                    self.height() - 1)
        self._drawCaption(qp, leftMargin=0)
        self._drawInfo(qp)
        self._drawOutputBoxes(qp)
        self._drawOutputs(qp)


    def resizeToZoomedSize(self):
        self.setGeometry(self._zoom(self._unzoomedX),
                         self._zoom(self._unzoomedY),
                         self._zoom(self.WIDTH),
                         self._zoom(self.HEIGHT + self._xoverObject.numOutputs() * self.OUTPUT_HEIGHT))
        try:
            self._resizeCaptionButtons()
        except AttributeError:
            pass


    def outputDotRects(self):
        ret = []
        zoh = self._zoom(self.OUTPUT_HEIGHT)
        # calculate initial dot coordinates
        dx = self.width() - self._zoom(2 * self.DOT_RADIUS)
        dy = self._zoom(self.HEIGHT + 6)
        dw = self._zoom(2 * self.DOT_RADIUS) - 1
        dh = dw
        for idx in range(0, self._xoverObject.numChannels()):
            ret.append(QRect(dx, dy, dw, dh))
            dy += zoh
        return ret


    def _addCaptionButtons(self):
        # edit button
        self._editButton = QToolButton(self)
        icon = QIcon()
        icon.addPixmap(QPixmap(":/MainWindow/edit.png"), QIcon.Normal, QIcon.Off)
        self._editButton.setStyleSheet(self.BUTTON_STYLESHEET)
        self._editButton.setIcon(icon)
        self._editButton.setToolTip("Edit...")
        # noinspection PyUnresolvedReferences
        self._editButton.clicked.connect(self.onEdit)
        # resize buttons
        self.resizeToZoomedSize()


    def _buttonsRightMargin(self):
        return self._zoom(self.DOT_RADIUS)


    def _drawInfo(self, qp):
        font = QFont(self.INFO_FONT[0], self._zoom(self.INFO_FONT[1]), self.INFO_FONT[2])
        font.setItalic(True)
        font2 = QFont(self.INFO_FONT[0], self._zoom(self.INFO_FONT[1]), self.INFO_FONT[2])
        qp.setFont(font)
        metrics = qp.fontMetrics()
        fontHeight = metrics.height()
        l = self._zoom(self.INFO_LEFTMARGIN)
        t = self._zoom(self.CAPTION_HEIGHT + self.INFO_TOPMARGIN) + 1
        w = self._zoom(self.WIDTH - 2 * self._zoom(self.CAPTION_LEFTMARGIN))
        h = fontHeight

        qp.setPen(self.INFO_COLOR)
        qp.setFont(font)
        qp.drawText(l, t, w, h, Qt.AlignLeft | Qt.AlignTop, "Input Channels:")
        metrics = qp.fontMetrics()
        l += metrics.width("Input Channels: ")
        qp.setPen(self.INFO_COLOR2)
        qp.setFont(font2)
        qp.drawText(l, t, w, h, Qt.AlignLeft | Qt.AlignTop, "%d" % self._xoverObject.numChannels())

        t += int(round(h * 1.15))
        l = self._zoom(self.INFO_LEFTMARGIN)
        qp.setPen(self.INFO_COLOR)
        qp.setFont(font)
        qp.drawText(l, t, w, h, Qt.AlignLeft | Qt.AlignTop, "Input System:")
        t += int(round(h * 1.15))
        l = self._zoom(self.INFO_LEFTMARGIN)
        qp.setPen(self.INFO_COLOR2)
        qp.setFont(font2)
        typeString = ""
        for text, type_ in INPUT_TYPE_MAPPING.items():
            if self._xoverObject.inputType() == type_:
                typeString = text
        qp.drawText(l, t, w, h, Qt.AlignLeft | Qt.AlignTop, "  " + typeString)


    def _drawOutputBoxes(self, qp):
        zoh = self._zoom(self.OUTPUT_HEIGHT)
        # calculate initial box coordinates
        bx = self.width() - self._zoom(self.DOT_RADIUS + self.MARKER_WIDTH)
        by = self._zoom(self.HEIGHT)
        bw = self._zoom(self.MARKER_WIDTH) - 1
        bh = zoh - 1
        for idx in range(0, self._xoverObject.numChannels()):
            # draw color box
            qp.setPen(self.LINECOLOR)
            qp.setBrush(colorForChannelNum(idx))
            qp.drawRect(bx, by, bw, bh)
            by += zoh


    def _drawOutputDots(self, qp):
        zoh = self._zoom(self.OUTPUT_HEIGHT)
        # calculate initial dot coordinates
        dx = self.width() - self._zoom(2 * self.DOT_RADIUS)
        dy = self._zoom(self.HEIGHT + 0.5 * self.DOT_RADIUS + 2)
        dw = self._zoom(2 * self.DOT_RADIUS)
        dh = dw
        for idx in range(0, self._xoverObject.numChannels()):
            # draw output dot
            pen = QPen(self.DOT_LINECOLOR)
            pen.setWidth(2)
            qp.setPen(pen)
            if self._xoverObject.hasOutputConnectionAt(idx):
                qp.setBrush(self.DOT_BGCOLOR_CONNECTED)
                qp.drawEllipse(dx, dy, dw, dh)
            else:
                qp.drawArc(dx, dy, dw, dh, 90 * 16, -180 * 16)
            dy += zoh


    def _drawOutputs(self, qp):
        font = QFont(self.INFO_FONT[0], self._zoom(self.INFO_FONT[1]), self.INFO_FONT[2])
        qp.setFont(font)
        zoh = self._zoom(self.OUTPUT_HEIGHT)
        # calculate initial line coordinates
        lx1 = 0
        lx2 = self.width() - self._zoom(1 * self.DOT_RADIUS) - 1
        ly = self._zoom(self.HEIGHT)
        # calculate initial text coordinates
        tx = self._zoom(self.INFO_LEFTMARGIN)
        ty = self._zoom(self.HEIGHT + 2)
        metrics = qp.fontMetrics()
        fontHeight = metrics.height()
        w = self._zoom(self.WIDTH - 2 * self.CAPTION_LEFTMARGIN)
        h = fontHeight
        for idx in range(0, self._xoverObject.numChannels()):
            # draw divider line
            qp.setPen(self.LINECOLOR)
            qp.drawLine(lx1, ly, lx2, ly)
            ly += zoh
            # draw channel name
            qp.setPen(self.INFO_COLOR2)
            qp.drawText(tx, ty, w, h, Qt.AlignLeft | Qt.AlignTop, PA_CHANNEL_NAMES[idx])
            ty += zoh


    def _resizeCaptionButtons(self):
        margin = int(round(self._zoom(self.CAPTION_HEIGHT - self.BUTTON_HEIGHT) / 2))
        rightMargin = self._buttonsRightMargin()
        buttonHeight = self._zoom(self.BUTTON_WIDTH)
        buttonWidth = self._zoom(self.BUTTON_HEIGHT)
        self._editButton.setGeometry(
                self._zoom(self.WIDTH - self.BUTTON_WIDTH) - margin * 2 - rightMargin,
                margin,
                buttonHeight,
                buttonWidth)
