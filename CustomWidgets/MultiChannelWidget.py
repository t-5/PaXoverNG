from PyQt5.QtCore import QRect, Qt
from PyQt5.QtGui import QFont, QPen

from CustomWidgets.XoverWidget import XoverWidget


class MultiChannelWidget(XoverWidget):

    def __init__(self, parent, xoverObject, x=0, y=0):
        super().__init__(parent=parent, xoverObject=xoverObject, x=x, y=y)


    def filterName(self):
        return self._xoverObject.filterName()


    def inputDotRects(self):
        ret = []
        zoh = self._zoom(self.OUTPUT_HEIGHT)
        # calculate initial dot coordinates
        dx = 0
        dy = self._zoom(self.HEIGHT + 6)
        dw = self._zoom(2 * self.DOT_RADIUS) - 1
        dh = dw
        for idx in range(0, self._xoverObject.numInputs()):
            ret.append(QRect(dx, dy, dw, dh))
            dy += zoh
        return ret


    def onEdit(self):
        raise NotImplementedError("Abstract method MultiChannelWidget::onEdit() called.")


    @staticmethod
    def outputWidgets():
        return ()


    def outputDotRects(self):
        ret = []
        zoh = self._zoom(self.OUTPUT_HEIGHT)
        # calculate initial dot coordinates
        dx = self.width() - self._zoom(2 * self.DOT_RADIUS)
        dy = self._zoom(self.HEIGHT + 6)
        dw = self._zoom(2 * self.DOT_RADIUS) - 1
        dh = dw
        for idx in range(0, self._xoverObject.numOutputs()):
            ret.append(QRect(dx, dy, dw, dh))
            dy += zoh
        return ret


    def resizeToZoomedSize(self):
        self.setGeometry(self._zoom(self._unzoomedX),
                         self._zoom(self._unzoomedY),
                         self._zoom(self.WIDTH),
                         self._zoom(self.HEIGHT + max(self._xoverObject.numInputs(), self._xoverObject.numOutputs()) * self.OUTPUT_HEIGHT))
        self._resizeCaptionButtons()


    def _buttonsRightMargin(self):
        return self._zoom(self.DOT_RADIUS)


    def _drawInputAndOutputDots(self, qp):
        zoh = self._zoom(self.OUTPUT_HEIGHT)
        # calculate initial dot coordinates
        dx = self.width() - self._zoom(2 * self.DOT_RADIUS)
        dw = self._zoom(2 * self.DOT_RADIUS)
        dh = dw

        dy = self._zoom(self.HEIGHT + 0.5 * self.DOT_RADIUS + 2)
        for idx in range(0, self._xoverObject.numOutputsForDrawing()):
            # draw output dot
            pen = QPen(self.DOT_LINECOLOR)
            pen.setWidth(2)
            qp.setPen(pen)
            if self._xoverObject.hasOutputConnectionAt(idx):
                qp.setBrush(self.DOT_BGCOLOR_CONNECTED)
                qp.drawEllipse(dx, dy, dw, dh)
            else:
                qp.drawArc(dx, dy, dw, dh, 90 * 16, -180 * 16)
            dy += zoh

        dy = self._zoom(self.HEIGHT + 0.5 * self.DOT_RADIUS + 2)
        for idx in range(0, self._xoverObject.numInputs()):
            # draw output dot
            pen = QPen(self.DOT_LINECOLOR)
            pen.setWidth(2)
            qp.setPen(pen)
            if self._xoverObject.hasInputConnectionAt(idx):
                qp.setBrush(self.DOT_BGCOLOR_CONNECTED)
                qp.drawEllipse(0, dy, dw, dh)
            else:
                qp.drawArc(0, dy, dw, dh, 90 * 16, 180 * 16)
            dy += zoh


    def _drawInputsAndOutputs(self, qp):
        font = QFont(self.INFO_FONT[0], self._zoom(self.INFO_FONT[1]), self.INFO_FONT[2])
        qp.setFont(font)
        zoh = self._zoom(self.OUTPUT_HEIGHT)
        # calculate initial line coordinates
        lx1 = self._zoom(self.DOT_RADIUS) + 1
        lx2 = self._separatorLineEnd()
        # calculate initial text coordinates
        metrics = qp.fontMetrics()
        fontHeight = metrics.height()
        w = self._zoom(self.WIDTH - 2 * self.CAPTION_LEFTMARGIN)
        h = fontHeight

        numInputs = self._xoverObject.numInputs()
        numOutputs = self._xoverObject.numOutputsForDrawing()

        # draw divider lines
        ly = self._zoom(self.HEIGHT)
        for idx in range(0, max(numInputs, numOutputs)):
            qp.setPen(self.LINECOLOR)
            qp.drawLine(lx1, ly, lx2, ly)
            ly += zoh
        # draw input channel names
        ty = self._zoom(self.HEIGHT + 2)
        for idx in range(0, numInputs):
            qp.setPen(self.INFO_COLOR2)
            qp.drawText(self._zoom(2 * self.DOT_RADIUS), ty, 100, h, Qt.AlignLeft | Qt.AlignTop, "%d" % (idx + 1))
            ty += zoh
        # draw output channel names
        ty = self._zoom(self.HEIGHT + 2)
        for idx in range(0, numOutputs):
            qp.setPen(self.INFO_COLOR2)
            qp.drawText(w - 100, ty, 100, h, Qt.AlignRight | Qt.AlignTop, "%d" % (idx + 1))
            ty += zoh


    def _separatorLineEnd(self):
        return self.width() - self._zoom(self.DOT_RADIUS) - 1