from PyQt5.QtCore import QRect, Qt
from PyQt5.QtGui import QColor, QFont

from CustomWidgets.MultiChannelWidget import MultiChannelWidget
from WindowClasses.EditMixerDialog import EditMixerDialog


class MixerWidget(MultiChannelWidget):

    LINECOLOR = QColor(180, 255, 255)
    CAPTION_BGCOLOR = QColor(0, 102, 102)
    HEIGHT = 66
    WIDTH = 150


    def __init__(self, parent, xoverObject, x=0, y=0):
        super().__init__(parent=parent, xoverObject=xoverObject, x=x, y=y)
        self._frdButton = None
        self._frdDialog = None
        self._addCaptionButtons()


    def inputDotRects(self):
        ret = []
        zoh = self._zoom(self.OUTPUT_HEIGHT)
        # calculate initial dot coordinates
        dx = 0
        dy = self._zoom(self.HEIGHT + 6)
        dw = self._zoom(2 * self.DOT_RADIUS) - 1
        dh = dw
        for idx in range(0, self._xoverObject.numInputs()):
            ret.append(QRect(dx, dy, dw, dh))
            dy += zoh
        return ret


    def onEdit(self):
        dlg = EditMixerDialog(self._mainWindow, self._xoverObject)
        dlg.exec()
        dlg.deleteLater()
        self.mainWindow().markDirty()
        self.repaint()


    def paint(self, qp):
        self._drawInputAndOutputDots(qp)
        # overall rectangle
        qp.setPen(self.LINECOLOR)
        qp.setBrush(self.BGCOLOR)
        qp.drawRect(self._zoom(self.DOT_RADIUS) + 1,
                    0,
                    self.width() - 2 - self._zoom(2 * self.DOT_RADIUS),
                    self.height() - 1)
        self._drawCaption(qp)
        self._drawInfo(qp)
        self._drawInputsAndOutputs(qp)


    def resetFrdDialog(self):
        self._frdDialog = None


    def updateFrequencyResponseLabel(self):
        pass # TODO: needed?


    def _drawInfo(self, qp):
        # fonts & metrics
        font = QFont(self.INFO_FONT[0], self._zoom(self.INFO_FONT[1]), self.INFO_FONT[2])
        font2 = QFont(self.INFO_FONT[0], self._zoom(self.INFO_FONT[1]), self.INFO_FONT[2])
        qp.setFont(font)
        metrics = qp.fontMetrics()
        # calculate positions and dimensions
        x = self._zoom(self.INFO_LEFTMARGIN + self.DOT_RADIUS + 3)
        y = self._zoom(self.CAPTION_HEIGHT + 5)
        h = metrics.height()
        maxW = self._zoom(self.WIDTH - 2 * self.INFO_LEFTMARGIN - 2 * self.DOT_RADIUS)

        qp.setPen(self.INFO_COLOR)
        s = "Input channels:"
        w = metrics.width(s)
        qp.drawText(x, y, maxW, h, Qt.AlignLeft | Qt.AlignTop, s)
        x += w + 3
        maxW -= w
        qp.setFont(font2)
        qp.setPen(self.INFO_COLOR2)
        s = "%d" % self._xoverObject.numInputs()
        qp.drawText(x, y, maxW, h, Qt.AlignLeft | Qt.AlignTop, s)

        maxW = self._zoom(self.WIDTH - 2 * self.INFO_LEFTMARGIN - 2 * self.DOT_RADIUS)
        x = self._zoom(self.INFO_LEFTMARGIN + self.DOT_RADIUS + 3)
        y += h

        qp.setFont(font)
        qp.setPen(self.INFO_COLOR)
        s = "Output channels:"
        w = metrics.width(s)
        qp.drawText(x, y, maxW, h, Qt.AlignLeft | Qt.AlignTop, s)
        x += w + 3
        maxW -= w
        qp.setFont(font2)
        qp.setPen(self.INFO_COLOR2)
        s = "%d" % self._xoverObject.numOutputs()
        qp.drawText(x, y, maxW, h, Qt.AlignLeft | Qt.AlignTop, s)
