from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor, QFont
from PyQt5.QtWidgets import QLabel, QToolButton

from CustomWidgets.MultiChannelWidget import MultiChannelWidget
from WindowClasses.EditFilterDialog import EditFilterDialog
from WindowClasses.FrdComparisonDialog import FrdComparisonDialog


class FilterWidget(MultiChannelWidget):

    LINECOLOR = QColor(180, 180, 255)
    CAPTION_BGCOLOR = QColor(0, 0, 102)
    HEIGHT = 176
    WIDTH = 270
    CHANNEL_BUTTON_SIZE = 18


    def __init__(self, parent, xoverObject, x=0, y=0):
        super().__init__(parent=parent, xoverObject=xoverObject, x=x, y=y)
        self._frdButton = None
        self._frdDialog = None
        self._addChannelButton = None
        self._removeChannelButton = None
        self._addCaptionButtons()
        self._addChannelButtons()
        self._frequencyResponseLabel = QLabel("", self)
        self._frequencyResponseLabel.setScaledContents(True)
        self.updateFrequencyResponseLabel()
        self._resizeFrequencyResponseLabel()
        self.placeChannelButtons()
        self._resizeChannelButtons()


    def onAddChannel(self):
        self._xoverObject.addChannel()
        self.placeChannelButtons()
        self.mainWindow().repaintDrawArea()


    def onEdit(self):
        dlg = EditFilterDialog(self._mainWindow, self._xoverObject)
        dlg.exec()
        self.updateFrequencyResponseLabel()
        dlg.deleteLater()
        self.mainWindow().markDirty()


    def onFrdDialog(self):
        if self._frdDialog is None:
            self._frdDialog = FrdComparisonDialog(self._mainWindow, self._xoverObject)
            self._frdDialog.show()
            self._mainWindow.addFrdDialog(self._frdDialog)
        else:
            self._frdDialog.show()
            self._frdDialog.raise_()
            self._frdDialog.activateWindow()


    def onRemoveChannel(self):
        self._xoverObject.removeChannel()
        self.placeChannelButtons()
        self.mainWindow().repaintDrawArea()


    def paint(self, qp):
        self._drawInputAndOutputDots(qp)
        # overall rectangle
        qp.setPen(self.LINECOLOR)
        qp.setBrush(self.BGCOLOR)
        qp.drawRect(self._zoom(self.DOT_RADIUS) + 1,
                    0,
                    self.width() - 2 - self._zoom(2 * self.DOT_RADIUS),
                    self.height() - 1)
        # graph rectangle
        qp.setBrush(self.GRAPH_BGCOLOR)
        qp.drawRect(self._zoom(self.DOT_RADIUS) + 1,
                    self._zoom(self.GRAPH_Y - 8),
                    self.width() - 2 - self._zoom(2 * self.DOT_RADIUS),
                    self._zoom(self.GRAPH_HEIGHT + 12))
        self._drawCaption(qp)
        self._drawInputsAndOutputs(qp)
        self._drawInfo(qp)
        self._showChannelButtons()


    def placeChannelButtons(self):
        sz = self._zoom(self.CHANNEL_BUTTON_SIZE)
        self._removeChannelButton.setFixedSize(sz, sz)
        self._addChannelButton.setFixedSize(sz, sz)
        self._showChannelButtons()


    def resetFrdDialog(self):
        self._frdDialog = None


    def resizeToZoomedSize(self):
        super().resizeToZoomedSize()
        self._resizeCaptionButtons()
        self._resizeChannelButtons()
        self._resizeFrequencyResponseLabel()


    def updateFrequencyResponseLabel(self):
        self._frequencyResponseLabel.setPixmap(self._xoverObject.frequencyResponsePixmap())


    # noinspection PyUnresolvedReferences
    def _addChannelButtons(self):
        # remove channel button
        self._removeChannelButton = QToolButton(self)
        self._removeChannelButton.setText("-")
        self._removeChannelButton.setToolTip("Remove this channel")
        self._removeChannelButton.clicked.connect(self.onRemoveChannel)
        self._removeChannelButton.setStyleSheet("background-color: #aaaaaa; color: #660000; font-size: 14px; font-weight: bold; padding: 2px; border: 1px solid #aa0000;")
        self._removeChannelButton.hide()
        # add channel button
        self._addChannelButton = QToolButton(self)
        self._addChannelButton.setText("+")
        self._addChannelButton.setToolTip("Add a channel")
        self._addChannelButton.clicked.connect(self.onAddChannel)
        self._addChannelButton.setStyleSheet("background-color: #aaaaaa; color: #000000; font-size: 14px; font-weight: bold; padding: 2px; border: 1px solid #00aa00;")
        self._addChannelButton.hide()


    def _drawInfo(self, qp):
        # fonts & metrics
        font = QFont(self.INFO_FONT[0], self._zoom(self.INFO_FONT[1]), self.INFO_FONT[2])
        font2 = QFont(self.INFO_FONT[0], self._zoom(self.INFO_FONT[1]), self.INFO_FONT[2])
        font3 = QFont(self.INFO_FONT[0], self._zoom(self.INFO_FONT[1]), self.INFO_FONT[2])
        font3.setBold(True)
        qp.setFont(font)
        metrics = qp.fontMetrics()
        # calculate positions and dimensions
        x = self._zoom(self.INFO_LEFTMARGIN + self.DOT_RADIUS)
        y = self._zoom(self.CAPTION_HEIGHT + self.GRAPH_HEIGHT + 16)
        h = metrics.height()
        maxW = self._zoom(self.WIDTH - 2 * self.INFO_LEFTMARGIN - 2 * self.DOT_RADIUS)
        # draw filter name
        qp.setFont(font3)
        qp.setPen(self.INFO_COLOR2)
        qp.drawText(x, y, maxW, h, Qt.AlignLeft | Qt.AlignTop, self.filterName())
        y += h + self._zoom(1)
        # draw text info
        i = 0
        # split info into outer parts and cycle through them
        outerInfoParts = self._xoverObject.info().split("|")
        for outerInfoPart in outerInfoParts:
            # split outer info part into inner parts
            infoParts = outerInfoPart.split(":")
            # draw inner part 1
            qp.setFont(font)
            qp.setPen(self.INFO_COLOR)
            if i > 0:
                s = "|"
            else:
                s = ""
            s += infoParts[0] + ":"
            w = metrics.width(s)
            qp.drawText(x, y, maxW, h, Qt.AlignLeft | Qt.AlignTop, s)
            x += w
            maxW -= w
            # draw inner part 2
            qp.setFont(font2)
            qp.setPen(self.INFO_COLOR2)
            w = metrics.width(infoParts[1])
            qp.drawText(x, y, maxW, h, Qt.AlignLeft | Qt.AlignTop, infoParts[1])
            x += w
            maxW -= w
            # finally increment i
            i += 1


    def _resizeChannelButtons(self):
        channels = max(self._xoverObject.numInputs(), self._xoverObject.numOutputs())
        w = self._zoom(self.WIDTH)
        bw = self._zoom(self.CHANNEL_BUTTON_SIZE)
        x = int(round(w - bw) / 2)
        y = self._zoom(self.HEIGHT + (channels - 1) * self.OUTPUT_HEIGHT) + 1
        try:
            self._addChannelButton.setGeometry(x, y, bw, bw)
            self._removeChannelButton.setGeometry(x, y, bw, bw)
        except AttributeError:
            pass


    def _showChannelButtons(self):
        inputs = self._xoverObject.inputs()
        outputs = self._xoverObject.outputs()
        # check if last row is unconnected and there is more than one channel => show remove channel button
        if len(inputs) == 1 and inputs[-1] is None and outputs[-1] is None:
            self._removeChannelButton.hide()
            self._addChannelButton.show()
        elif inputs[-1] is not None or outputs[-1] is not None:
            self._removeChannelButton.hide()
            self._addChannelButton.show()
        else:
            self._removeChannelButton.show()
            self._addChannelButton.hide()
